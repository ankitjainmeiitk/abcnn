
def get_feed_batch(inp_feed, batch_size=100):
  "get batches from feed"

  import numpy 
  key = list(inp_feed.keys())[0]
  N = len(inp_feed[key])
  if(N <= batch_size):
    return inp_feed
  
  indices = numpy.random.permutation(N)
  out_feed = {}
  for key in list(inp_feed.keys()):
    x = inp_feed[key]
    out_feed[key] = inp_feed[key][indices[:batch_size]]  
  
  return out_feed



def get_feed_data(file):
  'get list of feeds from list of entries'
  import numpy 
  
  f = open(file, "r")
  lines = f.readlines()
  f.close()
  
  # pre-allocate memory
  SGs = numpy.zeros([len(lines),1])
  FEs = numpy.zeros([len(lines),1])
  weights = numpy.zeros([len(lines),1])
  specie_wyckoffs = numpy.zeros([len(lines), 118, 27]) 
  names = numpy.empty([len(lines),1], dtype="S500")
  
  # get all data  
  for i in range(len(lines)):
    line = lines[i]
    val = line.split()
    names[i][0] = val[0]
    SGs[i][0] = int(val[1])
    wyckoffs = [(int(_)-1) for _ in val[2].split("_")]
    species = [(int(_)-1) for _ in val[3].split("_")]
    for j in range(len(wyckoffs)):
      specie_wyckoffs[i][species[j]][wyckoffs[j]] += 1
    FEs[i][0] = float(val[4])
    
    if(len(val) == 6):
      weights[i][0] = float(val[5])
    else:
      weights[i][0] = 1.0

  # final dataset
  dataset = {'SG' : SGs, 'FE' : FEs, \
             'weight' : weights, \
             'specie_wyckoff_matrix' : specie_wyckoffs, \
             'name' : names, \
             }

  return dataset


def get_args():
  import argparse
  import sys

  parser = argparse.ArgumentParser(description='U-apI Neural Network')
  
  parser.add_argument("file", default = None, type = str, \
                          help = "data-file to process")
  parser.add_argument("-b", "--batch-size", default = 1024, type = int, \
                          help = "Batch-size in Training", dest = "batch_size")
  parser.add_argument("-e", "--epochs", default = 1000, \
                          type = int, help = "Number of Epochs for Training", \
                          dest = "epochs")
  parser.add_argument("-Ne", "--num-element", default = 4, type = int, \
                          help = "Vcetor dim for representing elements", \
                          dest = "num_element")
  parser.add_argument("-Nsg", "--num-spacegroup", default = 4, type = int, \
                          help = "Vcetor dim for representing spacegroups", \
                          dest = "num_sg")
  parser.add_argument("-Nw", "--num-wyckoff", default = 4, type = int, \
                          help = "Vcetor dim for representing Wyckoffs", \
                          dest = "num_wyckoff")
  parser.add_argument("-Nf1", "--num-filter1", default = 4, \
                          type = int, \
                          help = "Number of filters in convolution layer1", \
                          dest = "num_filter1")
  parser.add_argument("-Nf2", "--num-filter2", default = 4, \
                          type = int, \
                          help = "Number of filters in convolution layer2", \
                          dest = "num_filter2")
  parser.add_argument("-Nh1", "--num-hidden1", default = 20, type = int, \
                          help = "Number of Neurons in Hidden Layer1", \
                          dest = "num_hidden1")
  parser.add_argument("-Nh2", "--num-hidden2", default = 10, type = int, \
                          help = "Number of Neurons in Hidden Layer2", \
                          dest = "num_hidden2")
  parser.add_argument("-lr", "--learning-rate", default = 0.001, type = float, \
                          help = "Learning rate for Optimizer", \
                          dest = "learning_rate")
  parser.add_argument("-dr", "--drop-rate", default = 0.0, type = float, \
                          help = "Drop-rate for Regularization", \
                          dest = "drop_rate")
  parser.add_argument("-t", "--task", default = "train", type = str, \
                          help = "Task-Type: Train or Test or Predict", \
                          dest = "task", choices = ['train', 'test', 'predict'])
  parser.add_argument("-m", "--model", default = "model", type = str, \
                          help = "Path where model is saved or read from", \
                          dest = "path")
  
  args = parser.parse_args(sys.argv[1:])
  return args


if __name__ == "__main__":
  import tensorflow_NN
  import math
  
  # read arguments
  args = get_args()

  # get data
  feed = get_feed_data(args.file)


  # set the regressor
  regr = tensorflow_NN.NN(N_element = args.num_element, \
                          N_sg = args.num_sg, \
                          N_wyckoff = args.num_wyckoff, \
                          N_filter1 = args.num_filter1, \
                          N_filter2 = args.num_filter2, \
                          N_hidden1 = args.num_hidden1, \
                          N_hidden2 = args.num_hidden2, \
                          learning_rate = args.learning_rate, \
                          drop_rate = args.drop_rate, \
                          path = args.path \
                         )
 
  
  # initialize the regressor
  restore = regr.initialize()   
  if(args.task != 'train' and not restore):
    print("Not able to restore model!")
    import sys
    sys.exit()

  if(args.task == 'train'):
    if(restore):
      print(" "*80)
      print("="*80)
      print("Restarting from previous run")
      print(" "*80)
    else:
      print(" "*80)
      print("="*80)
      print("Starting from scratch")
      print(" "*80)
  
  print("Network Capacity is: " + str(regr.capacity))
  
  # do the task
  if(args.task == 'train'):
    num_samples = len(feed['FE'])
    num_batch = int(math.ceil(float(num_samples)/args.batch_size))
    for epoch in range(args.epochs):
      for batch in range(num_batch):
        regr.train(get_feed_batch(feed, batch_size=args.batch_size), \
                                                epoch, batch, num_batch)
  
  
  if(args.task == 'test'):
    regr.test(feed)
  
  if(args.task == 'predict'):
    regr.predict(feed)
