import tensorflow as tf

class NN():
  
  def __init__(self, N_element=None, 
                     N_sg= None, 
                     N_wyckoff= None, 
                     N_filter1=None, 
                     N_filter2=None, 
                     N_hidden1=None, 
                     N_hidden2=None, 
                     learning_rate=None, 
                     drop_rate=None, 
                     path=None):
                          
                          
    
    # set internal options
    self.max_batch = 10000
    self.num_element = 118
    self.num_wyckoff = 27
    self.num_sg = 230
    self.N_element = N_element
    self.N_sg = N_sg
    self.N_wyckoff = N_wyckoff
    self.N_filter1 = N_filter1
    self.N_filter2 = N_filter2
    self.N_hidden1 = N_hidden1
    self.N_hidden2 = N_hidden2
    self.learning_rate = learning_rate
    self.drop_rate = drop_rate
    self.path = path
    self.saver_file = self.path + "/model.ckpt"

    try:
      import os
      os.system("mkdir " + self.path)
    except:
      None

    # declare input
    self.inputs = {'specie_wyckoff_matrix' : tf.placeholder(tf.float32, 
                                      [None, self.num_element, 
                                      self.num_wyckoff]),
                  'SG' : tf.placeholder(tf.int32, [None, 1]), 
                  'FE' : tf.placeholder(tf.float32, [None, 1]),
                  'weight' : tf.placeholder(tf.float32, [None, 1]),
                  'is_training' : tf.placeholder(tf.bool, [1]), 
                 }
    self.count = tf.shape(self.inputs['FE'])[0]    
    #self.is_training = self.inputs['is_training'][0]  


    # declare weights 
    self.W_sg = tf.Variable(tf.truncated_normal(
                                [self.num_sg, self.N_sg]))
    self.W_element = tf.Variable(tf.truncated_normal( 
                                [self.num_element, self.N_element]))
    self.W_wyckoff = tf.Variable(tf.truncated_normal( 
                                [self.num_wyckoff * self.N_filter2, 
                                self.N_wyckoff]))

    self.filter1 = tf.Variable(tf.truncated_normal( 
                                [1, self.num_wyckoff, 1, self.N_filter1]))
    self.filter2 = tf.Variable(tf.truncated_normal( 
                                [1, self.num_wyckoff, self.N_filter1, 
                                self.N_filter2]))
    
    self.W_to_hidden1 = tf.Variable(tf.truncated_normal(
                                [self.N_sg * self.N_element * self.N_wyckoff, 
                                self.N_hidden1]))
    self.b_to_hidden1 = tf.Variable(tf.truncated_normal(
                                [self.N_hidden1]))
    self.W_hidden1_hidden2 = tf.Variable(tf.truncated_normal(
                                [self.N_hidden1, self.N_hidden2]))
    self.b_hidden1_hidden2 = tf.Variable(tf.truncated_normal(
                                [self.N_hidden2]))
    self.W_hidden2_out = tf.Variable(tf.truncated_normal([self.N_hidden2, 1]))
    self.b_hidden2_out = tf.Variable(tf.truncated_normal([1]))
    

    # get network capacity
    self.capacity = 0
    for var in tf.trainable_variables():
      shape = var.get_shape()
      var_parameters = 1
      for dim in shape:
        var_parameters *= dim.value
      self.capacity += var_parameters
        

    # set the model now  
    sg_rep = tf.matmul( 
                tf.reshape(
                  tf.one_hot(self.inputs['SG'], self.num_sg), 
                  [tf.shape(self.inputs['FE'])[0], self.num_sg]
                ), 
                self.W_sg
             )

    filter1_layer = self.activation( 
                      tf.nn.convolution(
                        tf.reshape(
                          self.inputs['specie_wyckoff_matrix'],
                          [-1, self.num_element, self.num_wyckoff, 1]
                        ), 
                        self.filter1, 
                        'SAME'
                      )
                   )     
    
    filter2_layer = self.activation( 
                      tf.nn.convolution(filter1_layer, self.filter2, 'SAME')
                   )     
    
    wyckoff_rep = self.activation( 
                    tf.reshape( 
                      tf.matmul( 
                        tf.reshape(
                          filter2_layer, 
                          [-1, self.num_wyckoff * self.N_filter2]
                        ), 
                        self.W_wyckoff
                      ), 
                      [-1, self.num_element, self.N_wyckoff]
                    ) 
                  )
    
    wyckoff_element_rep = self.activation( 
                           tf.reshape(
                             tf.matmul(
                               tf.reshape(
                                 tf.transpose(wyckoff_rep, perm=[0,2,1]),
                                 [-1, self.num_element]
                               ),
                               self.W_element
                             ), 
                             [-1, self.N_wyckoff, self.N_element]
                           )
                         )
    
    wyckoff_element_tile = tf.tile(
                            tf.reshape(
                              wyckoff_element_rep, 
                              [tf.shape(self.inputs['FE'])[0],
                                self.N_wyckoff, self.N_element, 1]
                            ), 
                            [1,1,1,self.N_sg]
                          )

    sg_tile = tf.tile(
                tf.reshape(
                  sg_rep,
                  [tf.shape(self.inputs['FE'])[0], 1, 1, self.N_sg]
                ),
                [1, self.N_wyckoff, self.N_element, 1]
              )
    
    final_rep = tf.reshape(
                  tf.multiply(wyckoff_element_tile, sg_tile),
                  [tf.shape(self.inputs['FE'])[0],
                          self.N_sg*self.N_element*self.N_wyckoff]
                )
    
    hidden1 = self.activation(
                tf.add(
                  tf.matmul(final_rep, self.W_to_hidden1),
                  self.b_to_hidden1
                )
              )
    #hidden1_dropout = tf.layers.dropout(hidden1, rate=self.drop_rate,
    #                                            training=self.is_training)
    
    hidden2 = self.activation(
                tf.add(
                  tf.matmul(hidden1, self.W_hidden1_hidden2),
                  self.b_hidden1_hidden2)
              )
    #hidden2_dropout = tf.layers.dropout(hidden2, rate=self.drop_rate,
    #                                            training=self.is_training)

    self.pred = tf.add(
                  tf.matmul(hidden2, self.W_hidden2_out),
                  self.b_hidden2_out
                )
    
    # set loss and optimizer                 
    self.op = tf.train.AdamOptimizer(learning_rate=self.learning_rate)
    self.saver = tf.train.Saver()
    
    self.MAE_loss = tf.losses.absolute_difference(self.inputs['FE'], self.pred, \
                      weights=self.inputs['weight'], scope=None, \
                      loss_collection=tf.GraphKeys.LOSSES, \
                      reduction=tf.losses.Reduction.MEAN)
    self.MSE_loss = tf.losses.mean_squared_error(self.inputs['FE'], self.pred, \
                      weights=self.inputs['weight'], scope=None, \
                      loss_collection=tf.GraphKeys.LOSSES, \
                      reduction=tf.losses.Reduction.MEAN)

    self.train_loss = self.MAE_loss
    self.test_loss = self.MAE_loss
    

  def initialize(self):
    self.train_op = self.op.minimize(self.train_loss)
    self.sess = tf.Session()
    self.sess.run(tf.global_variables_initializer())
   
    try:
      self.saver.restore(self.sess, self.saver_file)
      restore = True
    except:
      restore = False
    
    return restore
     

  def activation(self, inp):
    #return tf.nn.relu(inp)
    #return tf.nn.softplus(inp)
    #return tf.nn.softsign(inp)
    #return tf.nn.tanh(inp)
    #return tf.nn.elu(inp)
    return tf.sigmoid(inp)

  def translate_feed(self, input_map, start, end):
    if(end > len(input_map[list(input_map.keys())[0]])):
      end = len(input_map[list(input_map.keys())[0]])
    
    feed = {}
    for key in self.inputs:
      if(key is "is_training"):
        continue
      assert key in input_map, "Input key %s not found in given feed" % key
      feed[self.inputs[key]] = input_map[key][start:end]
    return feed
    
  
  
  def train(self, feed, epoch, batch, num_batch):
    loss = 0.0
    count = 0
    for start in range(0, len(feed[list(feed.keys())[0]]), self.max_batch):
      _feed = self.translate_feed(feed, start, start + self.max_batch)
      _feed[self.inputs['is_training']] = [True]
      _, _loss, _count = self.sess.run([self.train_op, self.train_loss, \
                                    self.count], feed_dict=_feed)
      loss += _count * _loss
      count += _count

    loss /= count
    if(epoch%10 == 0 and batch == 0):
      save_path = self.saver.save(self.sess, self.saver_file)
    if(batch == 0):
      print("Epoch: %d [%d/%d], Loss: %.6f" % (epoch, batch, num_batch, loss))


  def test(self, feed):
    loss = 0.0
    count = 0
    for start in range(0, len(feed[list(feed.keys())[0]]), self.max_batch):
      _feed = self.translate_feed(feed, start, start + self.max_batch)
      _feed[self.inputs['is_training']] = [False]
      _loss, _count = self.sess.run([self.test_loss, self.count], \
                                        feed_dict=_feed)
      loss += _count * _loss
      count += _count 
    loss /= count  
    print("Test Loss: %.6f" % (loss))
    
    return loss

  
  def predict(self, feed):
    inp = []
    pred = []
    for start in range(0, len(feed[list(feed.keys())[0]]), self.max_batch):
      _feed = self.translate_feed(feed, start, start + self.max_batch)
      _feed[self.inputs['is_training']] = [False]
      _inp, _pred = self.sess.run([self.inputs['FE'], self.pred], \
                                                              feed_dict=_feed)
      for _ in range(len(_inp)):
        inp.append(_inp[_][0])
        pred.append(_pred[_][0])  
      
    f = open("test_results.csv", "w+")
    for _ in range(len(inp)):
      f.write(str(inp[_]) + ", " + str(pred[_]) + "\n")
    f.close()

    import numpy
    inp = numpy.array(inp)
    pred = numpy.array(pred)
    diff = numpy.abs(numpy.subtract(inp, pred))
    print("Predict:: Min-Value: %.6f, Max-Value: %.6f, MAE: %.6f" % \
                                      (inp.min(), inp.max(), diff.mean()))
    
    return 

