# ABCNN
 
 _Atomic-positions independent material descriptor and an Attention-based Convolution Neural Network for machine learning of material properties._

### Installation

`cd FP_generator && pip3 install setup.py 'bdist_wheel' --user && cd ..`

**other requirements:** Tensorflow

## Usage:

#### Fingerprint-Generation

Step 1: `cd example`
 
Step 2: Generate properties.dat file listing structure-filenames and corresponding properties [structure files should be in the POSCAR format. All files listed in properties.dat file should be present in the example directory]

Step 3: Use `python3 FP.py` to convert from sturture to Wyckoffs-based material fingerprint

Step 4: `cd ..`


#### Machine Learning

Step 1: `cd model`

Step 2: `cp ./../example/fingerprint.dat ./`

Step 3: Use `python abcnn.py` to do machine learnine on materials



#### abcnn.py Usage:
`python abcnn.py [-h] [-b BATCH_SIZE] [-e EPOCHS] [-Ne NUM_ELEMENT]   [-Nsg NUM_SG] [-Nw NUM_WYCKOFF] [-Nf1 NUM_FILTER1]  [-Nf2 NUM_FILTER2] [-Nh1 NUM_HIDDEN1] [-Nh2 NUM_HIDDEN2]  [-lr LEARNING_RATE] [-dr DROP_RATE] [-t {train,test,predict}]  [-m PATH]  file`


```
usage: abcnn.py [-h] [-b BATCH_SIZE] [-e EPOCHS] [-Ne NUM_ELEMENT]
               [-Nsg NUM_SG] [-Nw NUM_WYCKOFF] [-Nf1 NUM_FILTER1]
               [-Nf2 NUM_FILTER2] [-Nh1 NUM_HIDDEN1] [-Nh2 NUM_HIDDEN2]
               [-lr LEARNING_RATE] [-dr DROP_RATE] [-t {train,test,predict}]
               [-m PATH]
               file

U-apI Neural Network

positional arguments:
  file                  data-file to process

optional arguments:
  -h, --help            show this help message and exit
  -b BATCH_SIZE, --batch-size BATCH_SIZE
                        Batch-size in Training
  -e EPOCHS, --epochs EPOCHS
                        Number of Epochs for Training
  -Ne NUM_ELEMENT, --num-element NUM_ELEMENT
                        Vcetor dim for representing elements
  -Nsg NUM_SG, --num-spacegroup NUM_SG
                        Vcetor dim for representing spacegroups
  -Nw NUM_WYCKOFF, --num-wyckoff NUM_WYCKOFF
                        Vcetor dim for representing Wyckoffs
  -Nf1 NUM_FILTER1, --num-filter1 NUM_FILTER1
                        Number of filters in convolution layer1
  -Nf2 NUM_FILTER2, --num-filter2 NUM_FILTER2
                        Number of filters in convolution layer2
  -Nh1 NUM_HIDDEN1, --num-hidden1 NUM_HIDDEN1
                        Number of Neurons in Hidden Layer1
  -Nh2 NUM_HIDDEN2, --num-hidden2 NUM_HIDDEN2
                        Number of Neurons in Hidden Layer2
  -lr LEARNING_RATE, --learning-rate LEARNING_RATE
                        Learning rate for Optimizer
  -dr DROP_RATE, --drop-rate DROP_RATE
                        Drop-rate for Regularization
  -t {train,test,predict}, --task {train,test,predict}
                        Task-Type: Train or Test or Predict
  -m PATH, --model PATH
                        Path where model is saved or read from
```
                        

**In case of errors, please contact Ankit Jain at ankitj@alumni.cmu.edu**