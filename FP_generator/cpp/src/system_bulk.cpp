#include "system_bulk.h" 


// ---------------------------------------------------------------------------//
// gettter for spacegroup
int SYSTEM_BULK::get_spacegroup(){
  return this->spaceGroupNumber;  
}



// ---------------------------------------------------------------------------//
void SYSTEM_BULK::get_species(int *num, string **species){
  if(this->species == NULL){
    *num = 0;
    return;
  }
  
  *num = this->num_species;
  *species = new string [this->num_species];
  for(int i=0; i<this->num_species; i++)
    (*species)[i] = this->species[i];

}


// ---------------------------------------------------------------------------//
void SYSTEM_BULK::get_wyckoff(int *num, string **wyckoff){
  if(this->wyckoff == NULL){
    *num = 0;
    return;
  }
  
  *num = this->num_wyckoff;
  *wyckoff = new string [this->num_wyckoff];
  for(int i=0; i<this->num_wyckoff; i++)
    (*wyckoff)[i] = this->wyckoff[i];

}

// ---------------------------------------------------------------------------//
// get num_atom_std / num_atom_primitive
int SYSTEM_BULK::get_std_primitive_natom_ratio(){
  if(this->crystal_coordinate_entry == NULL){
    cout<<"Crystal coordinates are not set!"<<'\n';
    return 1;
  }
  return this->crystal_coordinate_entry->num;
}


// ---------------------------------------------------------------------------//
// ............ get lattice permutations .................
void SYSTEM_BULK::get_alat_permutations(int *npermu, double **permu){

  double alat_permu[] = {
                        1, 0, 0,    0, 1, 0,    0, 0, 1,
                        1, 0, 0,    0, -1, 0,    0, 0, -1,
                        -1, 0, 0,    0, 1, 0,    0, 0, -1,
                        -1, 0, 0,    0, -1, 0,    0, 0, 1,
                        
                        0, 1, 0,    0, 0, 1,    1, 0, 0,
                        0, 1, 0,    0, 0, -1,    -1, 0, 0,
                        0, -1, 0,    0, 0, 1,    -1, 0, 0,
                        0, -1, 0,    0, 0, -1,    1, 0, 0,
                        
                        0, 0, 1,    1, 0, 0,    0, 1, 0,
                        0, 0, 1,    -1, 0, 0,    0, -1, 0,
                        0, 0, -1,    1, 0, 0,    0, -1, 0,
                        0, 0, -1,    -1, 0, 0,    0, 1, 0,
                        
                        0, 1, 0,    1, 0, 0,    0, 0, -1,
                        0, 1, 0,    -1, 0, 0,    0, 0, 1,
                        0, -1, 0,    1, 0, 0,    0, 0, 1,
                        0, -1, 0,    -1, 0, 0,    0, 0, -1,
                        
                        0, 0, 1,    0, 1, 0,    -1, 0, 0,
                        0, 0, 1,    0, -1, 0,    1, 0, 0,
                        0, 0, -1,    0, 1, 0,    1, 0, 0,
                        0, 0, -1,    0, -1, 0,    -1, 0, 0,
                        
                        1, 0, 0,    0, 0, 1,    0, -1, 0,
                        1, 0, 0,    0, 0, -1,    0, 1, 0,
                        -1, 0, 0,    0, 0, 1,    0, 1, 0,
                        -1, 0, 0,    0, 0, -1,    0, -1, 0
                     };


  *npermu = 24;
  *permu = new double [9*(*npermu)];
  for(int i=0; i<24; i++){
    for(int d=0;d<9;d++)
      (*permu)[9*i + d] = alat_permu[9*i+d];
  }

}

// ---------------------------------------------------------------------------//
// get symmetric wyckoff pairs of given pair
void SYSTEM_BULK::get_symmetric_wyckoff_pairs(string given_order, 
                                              int *num, string **orders){  
  
  struct_symmetric_wyckoff_pairs *helper_symmetric_wyckoff_pairs;
  struct_symmetric_orders *helper_symmetric_orders;
  
  helper_symmetric_wyckoff_pairs = 
                        this->symmetric_wyckoff_pairs[this->spaceGroupNumber-1];
        
  while(helper_symmetric_wyckoff_pairs->next != NULL){
    helper_symmetric_wyckoff_pairs = helper_symmetric_wyckoff_pairs->next;
    if(helper_symmetric_wyckoff_pairs->order == given_order){
            
      helper_symmetric_orders = 
                              helper_symmetric_wyckoff_pairs->symmetric_orders;
            
      *num = 0;      
      while(helper_symmetric_orders->next != NULL){
        helper_symmetric_orders = helper_symmetric_orders->next;
        (*num)++;
      }

      // memory allocate and store now
      *orders = new string [*num];
      helper_symmetric_orders = 
                              helper_symmetric_wyckoff_pairs->symmetric_orders;
            
      *num = 0;      
      while(helper_symmetric_orders->next != NULL){
        helper_symmetric_orders = helper_symmetric_orders->next;
        (*orders)[*num] = helper_symmetric_orders->order;
        (*num)++;
      }


      break;
    }
  };
  
  return;
}


// ---------------------------------------------------------------------------//
// see if two wyckoff-pairs are related using pre-clculated 
//      symmetric_wyckoff_pairs
bool SYSTEM_BULK::if_symmetric_wyckoff_pairs(string sorder1, string sorder2){  
 
  struct_symmetric_wyckoff_pairs *helper_symmetric_wyckoff_pairs;
  struct_symmetric_orders *helper_symmetric_orders;
  
  helper_symmetric_wyckoff_pairs = 
                        this->symmetric_wyckoff_pairs[this->spaceGroupNumber-1];
        
  while(helper_symmetric_wyckoff_pairs->next != NULL){
    helper_symmetric_wyckoff_pairs = helper_symmetric_wyckoff_pairs->next;
    if(helper_symmetric_wyckoff_pairs->order == sorder1){
            
      helper_symmetric_orders = 
                              helper_symmetric_wyckoff_pairs->symmetric_orders;
            
      while(helper_symmetric_orders->next != NULL){
        helper_symmetric_orders = helper_symmetric_orders->next;
        if(helper_symmetric_orders->order == sorder2){
          return true;
        }
      }

      break;
    }
  };

  return false;              
}


// ---------------------------------------------------------------------------//
// check if two wyckoff-orders are related through Wyckoff-site-symmetry
bool SYSTEM_BULK::if_wyckoff_order_related_through_symmetry(int num_wyckoff,
                                                         string sorder1, 
                                                         string sorder2,
                                                         bool screen, 
                                                         double *rotation,
                                                         double *translation){ 

  
  bool if_same;
  
  // convert string order into array of strings
  string *order1, *order2;
  split_string_into_string_array(sorder1, num_wyckoff, &order1);
  split_string_into_string_array(sorder2, num_wyckoff, &order2);

  //======================================================
  // screen based on symmetric wyckoff pairs
  if(screen){
   
    string temp_sorder1, temp_sorder2;
    
    for(int i=0; i<num_wyckoff; i++){
      for(int j=i+1; j<num_wyckoff; j++){
        
        temp_sorder1 = order1[i] + " " + order1[j];
        temp_sorder2 = order2[i] + " " + order2[j];

        if(!if_symmetric_wyckoff_pairs(temp_sorder1, temp_sorder2)){
          delete [] order1;
          delete [] order2;
          return false; 
        }


      } // over j
    } //over i
    
    
  } // screen



  //======================================================
  if_same = false;

  int npermu;
  double *permu;
  get_alat_permutations(&npermu, &permu);

  // ======================================================================== 
  // find multiplicity of sites
  int *wyckoff_multiplicity = new int [num_wyckoff]; 
  for(int i=0; i<num_wyckoff; i++){
    for(int j=0; j<this->wyckoff_entry->num; j++){
      if(order1[i] == this->wyckoff_entry->sites[j].symbol){
        wyckoff_multiplicity[i] = this->wyckoff_entry->sites[j].multiplicity;
        break; 
      }
    }
  }

  // find index of each wyckoff in sites list
  int *wyckoff_index = new int [num_wyckoff];
  wyckoff_index[0] = 0;
  for(int i=1;i<num_wyckoff; i++) 
    wyckoff_index[i] = wyckoff_index[i-1] + wyckoff_multiplicity[i-1];


  
  // ======================================================================== 
  // variable declarations
  int nsites = 0;
  for(int i=0; i<num_wyckoff; i++) nsites += wyckoff_multiplicity[i];
  double *sites1 = new double [12*nsites];
  double *sites2 = new double [12*nsites];
  
  bool if_solution;
  double origin_shift[3];
   
  for(int ip=0; ip<npermu; ip++){
 
    if(rotation != NULL){
      for(int d1=0; d1<3; d1++){
        for(int d2=0; d2<3; d2++){
          rotation[3*d1+d2] = permu[9*ip + 3*d1 + d2];
        }
      }
    }


    // get sites of each type now
    double *temp_sites1 = NULL;
    double *temp_sites2 = NULL;
    int temp_nsites;
    nsites = 0;
    for(int i=0; i<num_wyckoff; i++){ 
      
      get_wyckoff_site(order1[i], &temp_nsites, &temp_sites1, &permu[9*0], 
                                                                order1[i]);
      get_wyckoff_site(order2[i], &temp_nsites, &temp_sites2, &permu[9*ip],
                                                                order1[i]);
      for(int ii=0;ii<temp_nsites;ii++){
        for(int jj=0;jj<12;jj++){
          sites1[12*nsites + jj] = temp_sites1[12*ii+jj];
          sites2[12*nsites + jj] = temp_sites2[12*ii+jj];
        }
        nsites++;
      }

      delete [] temp_sites1; temp_sites1 = NULL;
      delete [] temp_sites2; temp_sites2 = NULL;
    }


    // now find all shifts that are possible
    set<int> set_unique_shift;
    set<int>::iterator it_unique_shift;
    double dtemp;
    int itemp;
    for(int i=0; i<num_wyckoff; i++){
      for(int ii=0; ii<wyckoff_multiplicity[i]; ii++){
        for(int jj=0; jj<wyckoff_multiplicity[i]; jj++){
          for(int c=0;c<3;c++){
            
            dtemp = sites1[12*wyckoff_index[i] + 12*ii + 4*c + 3] - 
                    sites2[12*wyckoff_index[i] + 12*jj + 4*c + 3];
            
            dtemp = dtemp - round(dtemp);
            if(dtemp < -this->tolerance) dtemp += 1.0;    
            itemp = round(dtemp * 1e+6);
            set_unique_shift.insert(itemp);
        
          }
        }
      }
    }

    int num_unique_shifts = set_unique_shift.size();
    double *unique_shifts = new double [num_unique_shifts];
    num_unique_shifts = 0;
    for(it_unique_shift = set_unique_shift.begin(); 
                  it_unique_shift != set_unique_shift.end();
                  ++it_unique_shift){

      unique_shifts[num_unique_shifts] = (*it_unique_shift) / (1.0e+6);
      num_unique_shifts++;
    
    }


    if_solution = false; 
    for(int tx=0; tx<num_unique_shifts; tx++){
      if(if_solution) break;

      for(int ty=0; ty<num_unique_shifts; ty++){
        if(if_solution) break;

        for(int tz=0; tz<num_unique_shifts; tz++){
          if(if_solution) break;

          if_solution = true;
          origin_shift[0] = unique_shifts[tx];
          origin_shift[1] = unique_shifts[ty];
          origin_shift[2] = unique_shifts[tz];

          if(translation != NULL){
            for(int d1=0; d1<3; d1++){
              translation[d1] = origin_shift[d1];
            }
          }

          // confirm if the solution is valid
          for(int ii=0; ii<num_wyckoff; ii++){

            if(!if_wyckoff_site_related_through_offset(wyckoff_multiplicity[ii], 
                    &sites1[12*wyckoff_index[ii]],
                    &sites2[12*wyckoff_index[ii]], origin_shift)){
              if_solution = false;

              break;
            }
          }

        }
      }
    }
    delete [] unique_shifts;
  
    if(if_solution){
      if_same = true;
      break;
    }

  }
  

  delete [] sites1;
  delete [] sites2;
  delete [] wyckoff_multiplicity;
  delete [] wyckoff_index;
  delete [] order1; order1 = NULL;
  delete [] order2; order2 = NULL;
  delete [] permu;
  

  return if_same;

}



// ---------------------------------------------------------------------------//
// get wyckoff sites
void SYSTEM_BULK::get_wyckoff_site(string wyckoff_site, int *num_entry, 
                     double **entry, double *permu, string allign_wyckoff_site){
 
  if(*entry != NULL){
    delete [] *entry;
    *entry = NULL;
  }
  
  struct_wyckoff_site *site;
  double temp1[12], temp2[12];
  int order[] = {0, 1, 2};  
  
  // find the allignemnt-site first
  site = NULL; 
  for(int i=0; i<this->wyckoff_entry->num; i++){
    if(allign_wyckoff_site == this->wyckoff_entry->sites[i].symbol) 
      site = &this->wyckoff_entry->sites[i]; 
  }
  
  // get coordinates of first site of allignment site
  for(int c=0;c<3;c++){
    for(int ic=0;ic<4; ic++){
      temp1[4*c+ic] = site->coordinates[12*0 + 4*c + ic];
    }
  }
   
  // find current site now
  site = NULL; 
  for(int i=0; i<this->wyckoff_entry->num; i++){
    if(wyckoff_site == this->wyckoff_entry->sites[i].symbol) 
      site = &this->wyckoff_entry->sites[i]; 
  }

  // find first entry of current site
  for(int d=0; d<12; d++) temp2[d] = 0.0;
  for(int c=0;c<3;c++){
    for(int ic=0;ic<4; ic++){
      for(int t=0;t<3;t++){
        temp2[4*c+ic] += permu[3*c + t] * site->coordinates[12*0 + 4*t + ic];
      }
    }
  }


  // not find the order
  // there are 6 combinations possible, try all  
  int orders[] = {
                  0, 1, 2,
                  1, 0, 2,
                  0, 2, 1,
                  1, 2, 0,
                  2, 0, 1,
                  2, 1, 0
                 };

  // find right order now
  bool if_valid_order;
  for(int i=0; i<6; i++){
    if_valid_order = true;
    
    for(int c=0;c<3;c++){
      if(! if_valid_order) break;
      
      for(int ic=0; ic<3;ic++){
    
        if( abs( abs(temp1[4*c + ic]) - abs(temp2[4*c + orders[3*i + ic]]) ) >
                                                     this->tolerance){
          if_valid_order = false;
          break;
        }
            
      }
    
    }

    if(if_valid_order){
      for(int d=0; d<3; d++) order[d] = orders[3*i + d];
      break; 
    }
  }


  *num_entry = site->multiplicity;
  *entry = new double [12 * (*num_entry)];
  
  for(int i=0; i<site->num; i++){
    for(int i1=0; i1<this->crystal_coordinate_entry->num; i1++){
      
      // copy the original coordinates
      for(int c=0;c<3;c++){
        for(int ic=0;ic<4; ic++){
          temp1[4*c+ic] = site->coordinates[12*i + 4*c + ic];
        }
        temp1[4*c+3] += this->crystal_coordinate_entry->coordinates[3*i1+c];
      }


      // now permute them
      for(int d=0;d<12;d++) temp2[d] = 0.0;
      for(int c=0; c<3; c++){
        for(int ic=0; ic<3; ic++){
          for(int t=0;t<3;t++){
            temp2[4*c + ic] += permu[3*c + t] * temp1[4*t + ic];
          }
        }
        for(int t=0; t<3; t++)
          temp2[4*c + 3] += permu[3*c + t] * temp1[4*t + 3];
      }

      // re-order them now
      for(int c=0;c<3; c++){
        for(int ic=0; ic<3; ic++){
          temp1[4*c+ic] = temp2[4*c+order[ic]];
        }
        temp1[4*c+3] = temp2[4*c+3];
      }

      // store them now  
      for(int c=0;c<3;c++){
        for(int ic=0; ic<4; ic++){
          (*entry)[12*this->crystal_coordinate_entry->num*i + 12*i1 + 4*c + ic]
              = temp1[4*c+ic];
        }
      }


    }
  }

}


// ---------------------------------------------------------------------------//
// check if two "wyckoff-site-distances" are same or not
bool SYSTEM_BULK::if_wyckoff_site_related_through_offset(
                                           int ndist, double *dist1, 
                                           double *dist2, double *origin_shift,
                                           int flip){


  if(flip > 7)
    return false;

  // flips are use in case we need x -> -x + 6; so to take care of -x
  int flips[] = {1, 1, 1,
                 1, 1, -1,
                 1, -1, 1,
                 -1, 1, 1,
                 -1, -1, 1,
                 -1, 1, -1,
                 1, -1, -1,
                 -1, -1, -1 };
    

  double offset[3];
  double A[9];
  double C[3];
  double X[3];
  bool if_valid, if_skip, if_found, if_solution;
  double diff;
  int rank;

  for(int i=0; i<ndist; i++){
    for(int j=0; j<ndist; j++){

      // make sure coeff of x,y,z are equal; else offset wont work
      if_valid = true;
      for(int c=0; c<3; c++){
        for(int ic=0; ic<3; ic++){
          if(abs(dist1[12*i + 4*c + ic] - 
                              flips[3*flip + ic] * dist2[12*j + 4*c + ic]) >
                    this->tolerance)
            if_valid = false;
        }
      }
      if(!if_valid) continue;
     

      // try to find offset that would be needed to allign i with j
      for(int ic=0; ic<3; ic++) offset[ic] = 0.0;
      for(int c=0; c<3; c++){
        for(int ic=0; ic<3; ic++){
          A[3*c + ic] = dist2[12*j + 4*c + ic];
        }
        C[c] = dist1[12*i + 4*c + 3] - 
                      (dist2[12*j + 4*c + 3] + origin_shift[c]);
      }
      
      solve_linear_system_of_reduced_rank(A, C, X, 3, 3, &rank, &if_solution); 
      
      for(int ic=0; ic<3; ic++) offset[ic] = X[ic];

      if(!if_solution) continue; 

      // check if this offset can help in matching of all coordinates
      if_skip = false;
      for(int ii=0; ii<ndist; ii++){
        if_found = false;
   
        for(int jj=0; jj<ndist; jj++){
          if_valid = true;
  
          
          for(int c=0; c<3; c++){
            for(int ic=0; ic<3; ic++){
              if(abs(dist1[12*ii + 4*c + ic] - 
                              flips[3*flip + ic] * dist2[12*jj + 4*c + ic]) >
                    this->tolerance)
              if_valid = false;
            }
          }
          if(!if_valid) continue;


          for(int c=0; c<3; c++){
            diff = 0.0;
            for(int ic=0; ic<3; ic++) diff += dist2[12*jj+4*c+ic] * offset[ic];
            diff += (dist2[12*jj+4*c+3] + origin_shift[c]) - dist1[12*ii+4*c+3];
            diff = diff - round(diff);
            
            if(abs(diff) > this->tolerance){
              if_valid = false;
              break;
            }
          }


          if(if_valid){
            if_found = true;
            break;
          }

        }
       
        
        if(!if_found){
          if_skip = true;
          break;
        }

      }
      if(!if_skip)  return true;

    }
  }

  
  return if_wyckoff_site_related_through_offset(ndist, dist1, dist2,
                                           origin_shift, flip + 1);

}


// ---------------------------------------------------------------------------//
// primitive POSCAR
string SYSTEM_BULK::get_primitive_poscar(){
  if(this->primitive_lattice == NULL){
    cout<<"Lattice not set!"<<'\n';
    return "";
  }
  return get_poscar_from_lattice(this->primitive_lattice);
}

// ---------------------------------------------------------------------------//
// primitive POSCAR
string SYSTEM_BULK::get_std_poscar(){
  if(this->std_lattice == NULL){
    cout<<"Lattice not set!"<<'\n';
    return "";
  }
  return get_poscar_from_lattice(this->std_lattice);
}

// ---------------------------------------------------------------------------//
// primitive natom
int SYSTEM_BULK::get_primitive_natom(){
  if(this->primitive_lattice == NULL){
    cout<<"Lattice not set!"<<'\n';
    return 0;
  }
  return this->primitive_lattice->natom;
}

// ---------------------------------------------------------------------------//
// std natom
int SYSTEM_BULK::get_std_natom(){
  if(this->std_lattice == NULL){
    cout<<"Lattice not set!"<<'\n';
    return 0;
  }
  return this->std_lattice->natom;
}


// ---------------------------------------------------------------------------//
// get standard lattice
void SYSTEM_BULK::calculate_std_or_primitive_lattice(
                                                LATTICE **lattice, int flag){
  // flag == 1 -> primitive
  // flag = -1 ->std

  if(this->lattice == NULL){
    cout<<"Looks like I do not have a lattice yet!"<<'\n';
    return;
  } 

  if(*lattice != NULL){
    delete *lattice;
    *lattice = NULL;
  }
  *lattice = new LATTICE;

  // call spglib
  double a[3][3];
  double (*position)[3] = new double [4*this->lattice->natom][3];
  int *types;
  types = new int [4*this->lattice->natom];

  for(int j=0;j<3;j++){
    for(int d=0;d<3;d++){
      a[d][j] = this->lattice->alat[3*j+d];
    }
  }
  for(int i=0;i<this->lattice->natom;i++){
    for(int d=0;d<3;d++){
      position[i][d] = this->lattice->basis[i].direct[d];
    }
    map_atomic_symbol_id(&types[i], &(this->lattice->basis[i].symbol), 1);
  }
 
  (*lattice)->natom = 0;
  double temp_tolerance = this->tolerance;

  
  while(temp_tolerance > 1e-8 && (*lattice)->natom == 0){
    if(flag == 1){
      (*lattice)->natom = spg_find_primitive(a, position, types, 
                                    this->lattice->natom, temp_tolerance);
    }
    if(flag == -1){
      (*lattice)->natom = spg_standardize_cell(a, position, types, 
                                 this->lattice->natom, 0, 0, temp_tolerance);
    }
    temp_tolerance *= 1e-1;
  }
  
  while(temp_tolerance < 1e-3 && (*lattice)->natom == 0){
    if(flag == 1){
      (*lattice)->natom = spg_find_primitive(a, position, types, 
                                    this->lattice->natom, temp_tolerance);
    }
    if(flag == -1){
      (*lattice)->natom = spg_standardize_cell(a, position, types, 
                                 this->lattice->natom, 0, 0, temp_tolerance);
    }
    temp_tolerance *= 1e+1;
  }

  if((*lattice)->natom == 0){
    cout<<"Spglib failed in finding primitive/std lattice!"<<'\n';
    delete [] position; position = NULL;
    delete [] types;    types = NULL;
    delete *lattice; *lattice = NULL;
    return;
  }

  // get alat
  for(int j=0;j<3;j++){
    for(int d=0;d<3;d++){
      (*lattice)->alat[3*j+d] = a[d][j];
    }
  }

  // get direct coord
  (*lattice)->basis = new ATOM [(*lattice)->natom];
  for(int i=0;i<(*lattice)->natom;i++){
    for(int d=0;d<3;d++){
      (*lattice)->basis[i].direct[d] = position[i][d];
    }
    map_atomic_symbol_id(&types[i], 
                  &((*lattice)->basis[i].symbol), -1);
  }

  // now get the cartesian coordinates as well
  set_inv_alat((*lattice));
  cart_to_direct((*lattice), -1);

  delete [] position; position = NULL;
  delete [] types; types = NULL;

}


// ---------------------------------------------------------------------------//
// check if lattice parameters are within the standard range
bool SYSTEM_BULK::if_valid_lattice_parameters_as_per_standardization(
                                                            double *cellpar){

  /*
    - Conventions as per: 
        -http://journals.iucr.org/a/issues/1984/03/00/a23047/a23047.pdf
        -The Standardization of Inorganic Crystal-Structure Data
        -E. PARTHI AND L. M. GELATO 
        -Acta Cryst. (1984). A40, 169-183 
  */


  // local copies of lattice parameters
  double b_a = cellpar[1];
  double c_a = cellpar[2];

  double alpha = cellpar[3];
  double beta = cellpar[4];
  double gamma = cellpar[5];

  // triclinic cells
  if(this->spaceGroupNumber <= 2){
    if(b_a < (1.0 + this->tolerance) || c_a < (b_a + this->tolerance) ){
      cout<<"Non-standard lengths for triclinic-cell!"<<'\n';
      return false;    
    }
    if( (alpha < (90 + this->tolerance) && 
                  (beta >= (90 - this->tolerance) || 
                      gamma >= (90 - this->tolerance)) ) || 
        (alpha >= (90 - this->tolerance) && 
                  (beta < (90 + this->tolerance) || 
                      gamma < (90 + this->tolerance)) ) ){
      cout<<"Non-standard angles for triclic-cell!"<<'\n';
      return false;
    }
  }
  
  // monoclinic cells
  if(this->spaceGroupNumber >= 3 && this->spaceGroupNumber <= 15){
    if(beta < (90+this->tolerance)){
      cout<<"Non-standard angles for monoclinic-cell!"<<'\n';
      return false;
    }
  
    int constraint_groups[] = {3,4,6,10,11};
    int num = sizeof(constraint_groups)/sizeof(int);
    for(int i=0; i<num; i++){
      if(this->spaceGroupNumber == constraint_groups[i] && 
            c_a < (1.0 - this->tolerance)){
        cout<<"Non-standard length for monoclinic-cell!"<<'\n';
        return false;
      }
    }

  }


  // orthorhombic cells
  if(this->spaceGroupNumber >= 16 && this->spaceGroupNumber <= 74){
    
    int constraint_groups_1[] = {17,18,20,21,25,27,32,34,35,37,42,43,44,45,49,
                                              50,55,56,58,59,65,66,67,68,72,74};
    int num_1 = sizeof(constraint_groups_1)/sizeof(int);
    for(int i=0; i<num_1; i++){
      if(this->spaceGroupNumber == constraint_groups_1[i] && 
            b_a < (1.0 - this->tolerance)){
        cout<<"Non-standard length for orthorhombic-cell!"<<'\n';
        return false;
      }
    }


    int constraint_groups_2[] = {16,19,22,23,24,47,48,61,69,70,71,73}; 
    int num_2 = sizeof(constraint_groups_2)/sizeof(int);
    for(int i=0; i<num_2; i++){
      if(this->spaceGroupNumber == constraint_groups_2[i] && 
          (c_a < (b_a - this->tolerance) || (b_a < (1.0 - this->tolerance)) ) ){
        cout<<"Non-standard length for orthorhombic-cell!"<<'\n';
        return false;
      }
    }

  }


  return true;
}


// ---------------------------------------------------------------------------//
// check if cellpar angles are valid
bool SYSTEM_BULK::if_valid_cellpar_angles(double *cellpar){
  // see "On the allowed values for the triclinic unit-cell angles, Foundations
  // of Crysttalography, Acta Cryst,A67, 93-95, 2011"
  
  double alpha = cellpar[3];
  double beta = cellpar[4];
  double gamma = cellpar[5];
    
  if(alpha + beta + gamma < 0) return false;
  if(alpha + beta + gamma > 360) return false;
  if(alpha + beta - gamma < 0) return false;
  if(alpha + beta - gamma > 360) return false;
  if(alpha - beta + gamma < 0) return false;
  if(alpha - beta + gamma > 360) return false;
  if(-alpha + beta + gamma < 0) return false;
  if(-alpha + beta + gamma > 360) return false;

  return true; 
}
  

// ---------------------------------------------------------------------------//
// get standard lattice
void SYSTEM_BULK::calculate_lattice(){
  
  if(this->lattice != NULL){
    delete this->lattice;
    this->lattice = NULL;
  }
  this->lattice = new LATTICE;

  // get lattice first
  double cellpar[6];
  if(this->spaceGroupNumber <= 2){
    for(int d=0; d<6; d++) cellpar[d] = this->lattice_parameter_values[d];
  }

  if(this->spaceGroupNumber >= 3 && this->spaceGroupNumber <= 15){
    for(int d=0; d<3; d++) cellpar[d] = this->lattice_parameter_values[d];
    cellpar[3] = 90; 
    cellpar[4] = this->lattice_parameter_values[3];
    cellpar[5] = 90;
  }

  if(this->spaceGroupNumber >= 16 && this->spaceGroupNumber <= 74){
    for(int d=0; d<3; d++) cellpar[d] = this->lattice_parameter_values[d];
    for(int d=3; d<6; d++) cellpar[d] = 90;
  }

  if(this->spaceGroupNumber >= 75 && this->spaceGroupNumber <= 142){
    cellpar[0] = this->lattice_parameter_values[0];
    cellpar[1] = 1.0;
    cellpar[2] = this->lattice_parameter_values[1];
    for(int d=3; d<6; d++) cellpar[d] = 90;
  }

  if(this->spaceGroupNumber >= 143 && this->spaceGroupNumber <= 167){
    if(this->trigonal_flag == "rhombohedron"){
      cellpar[0] = this->lattice_parameter_values[0]; 
      cellpar[1] = 1.0;
      cellpar[2] = 1.0;
      for(int d=3; d<6; d++) cellpar[d] = this->lattice_parameter_values[1]; 
    }
    if(this->trigonal_flag == "hexagonal"){
      cellpar[0] = this->lattice_parameter_values[0];
      cellpar[1] = 1.0;
      cellpar[2] = this->lattice_parameter_values[1];
      for(int d=3; d<5; d++) cellpar[d] = 90;
      cellpar[5] = 120;
    }
  }
 
  if(this->spaceGroupNumber >= 168 && this->spaceGroupNumber <= 194){
    cellpar[0] = this->lattice_parameter_values[0];
    cellpar[1] = 1.0;
    cellpar[2] = this->lattice_parameter_values[1];
    for(int d=3; d<5; d++) cellpar[d] = 90;
    cellpar[5] = 120;
  }
 
  if(this->spaceGroupNumber >= 195 && this->spaceGroupNumber <= 230){
    cellpar[0] = this->lattice_parameter_values[0];
    cellpar[1] = 1.0;
    cellpar[2] = 1.0;
    for(int d=3; d<6; d++) cellpar[d] = 90;
  }

  if( ! if_valid_cellpar_angles(cellpar) ){
    cout<<"Oops, looks like angles are not right! negative volume!"<<'\n';
    delete this->lattice;
    this->lattice = NULL;
    return;
  }

  /*
  if( ! if_valid_lattice_parameters_as_per_standardization(cellpar)){
    cout<<"Oops, lattice parameters does't seem to be in the standard foramt!";
    cout<<'\n';
    delete this->lattice;
    this->lattice = NULL;
    return;
  }
  */

  calculate_cellpar_from_alat(this->lattice->alat, cellpar, -1);
   

  // now get basis
  // count natom first
  this->lattice->natom = 0;
  for(int i=0; i<this->num_wyckoff; i++){
    for(int j=0; j<this->wyckoff_entry->num;j++){
      if(this->wyckoff_entry->sites[j].symbol == this->wyckoff[i]){
        this->lattice->natom += this->wyckoff_entry->sites[j].multiplicity;
        break;
      }
    }
  }
  this->lattice->basis = new ATOM [this->lattice->natom];
  
  // now get direct coordinates
  int temp_count = 0;
  double origin[3];
  char coord[] = {'x', 'y', 'z'};
  string key;
  double value;
  bool if_key_found;
  for(int i=0; i<this->num_wyckoff; i++){
    for(int j=0; j<this->wyckoff_entry->num; j++){
      if(this->wyckoff_entry->sites[j].symbol == this->wyckoff[i]){
        for(int cc=0; cc<this->crystal_coordinate_entry->num; cc++){
          for(int d=0;d<3;d++) origin[d] =
                    this->crystal_coordinate_entry->coordinates[3*cc+d];
          for(int e=0; e<this->wyckoff_entry->sites[j].num; e++){
           
            for(int d=0; d<3; d++)
                this->lattice->basis[temp_count].direct[d] = 0.0;
            
            for(int c=0; c<3; c++){
              for(int ic=0;ic<3;ic++){
                if(abs(this->wyckoff_entry->sites[j].coordinates[12*e+4*c+ic]) 
                                                          > this->tolerance){
                  key = coord[ic] + this->wyckoff[i] + int_to_str(i);
                  if_key_found = false;
                  for(int t=0; t<this->num_basis_parameters; t++){
                    if(key == this->basis_parameters[t]){
                      value = this->basis_parameter_values[t];
                      if_key_found = true; 
                    }
                  }
                  if(!if_key_found){
                    cout<<"Oops missing a parameter: "<<key<<'\n';
                    return;
                  }

                  this->lattice->basis[temp_count].direct[c] +=
                        this->wyckoff_entry->sites[j].coordinates[12*e+4*c+ic]
                        * value;
                }
              }
              this->lattice->basis[temp_count].direct[c] += origin[c] + 
                     this->wyckoff_entry->sites[j].coordinates[12*e + 4*c + 3];
            }
            this->lattice->basis[temp_count].symbol = this->species[i];
            temp_count++;
          
          }
        }
        break;
      } 
    }
  }


  // now get the cartesian coordinates as well
  set_inv_alat(this->lattice);
  cart_to_direct(this->lattice, -1);

}





// ---------------------------------------------------------------------------//
// set structure
void SYSTEM_BULK::set_structure_from_file(string file_content, 
                                                          bool reset_lattice){
 
  if(!reset_lattice && this->lattice == NULL){
    cout<<"Lattice not set!"<<'\n';
    return;
  }
  
  if(reset_lattice){
    if(this->lattice != NULL){
      delete this->lattice;
      this->lattice = NULL;
    }

    // read the structure
    this->structure_file_content = file_content;
    this->lattice = new LATTICE;
    read_structure(this->structure_file_content, this->lattice, 
                                                      this->tolerance);
    
    if(this->lattice->natom == 0){
      cout<<"Not able to read structure"<<'\n';
      return;
    }
  }


  // identify symmetry dataset of this structure
  if(this->symmetry_dataset != NULL) spg_free_dataset(this->symmetry_dataset);
  this->symmetry_dataset = calculate_symmetry_dataset(this->lattice);
  if(this->symmetry_dataset == NULL){
    return;
  }

  // COPY std-lattice vectors as wyckoff are based on std-basis
  //        std-basis is obtained as T[primitive-basis]
  //        where T is a transformation with Transformation and origin_shift
  for(int d1=0; d1<3;d1++){
    for(int d2=0; d2<3; d2++){
      this->lattice->alat[3*d1+d2] = 
                    this->symmetry_dataset->std_lattice[d1][d2];
    }
  }

  // set the spaceGroup
  this->set_spacegroup(this->symmetry_dataset->spacegroup_number, false); 
 
  // set the Wyckoff, wyckoff_coordinates, and species now
  set<int> set_unique;
  set<int>::iterator it_unique;
  for(int i=0; i<this->symmetry_dataset->n_atoms; i++) 
    set_unique.insert(this->symmetry_dataset->equivalent_atoms[i]);
  
  string symbol_wyckoff = "abcdefghijklmnopqrstuvwxyzA";
  int num = set_unique.size();
  string *wyckoff = new string [num];
  string *species = new string [num];
  double *wyckoff_coordinates = new double [3*num];

  int count = 0;
  for(it_unique = set_unique.begin(); it_unique != set_unique.end();
                                                      ++it_unique){
    
    wyckoff[count] = 
                symbol_wyckoff[this->symmetry_dataset->wyckoffs[*it_unique]];
    species[count] = this->lattice->basis[*it_unique].symbol;
    for(int d=0; d<3; d++)
      wyckoff_coordinates[3*count + d] =
                        this->lattice->basis[*it_unique].direct[d];
                 
    count++;
  }

  
  //===============================================
  double temp_coord[3];
  for(int i=0; i<num; i++){
    for(int d=0;d<3;d++) temp_coord[d] = 0.0;
    for(int d1=0;d1<3;d1++){
      for(int d2=0;d2<3;d2++){
        temp_coord[d1] += this->symmetry_dataset->transformation_matrix[d1][d2]
                            * wyckoff_coordinates[3*i + d2];
      }
    }
    for(int d=0;d<3;d++) wyckoff_coordinates[3*i+d] = temp_coord[d];
    for(int d=0;d<3;d++) wyckoff_coordinates[3*i+d] +=
                            this->symmetry_dataset->origin_shift[d];
    
  }

  this->set_wyckoff(num, wyckoff, false);
  this->calculate_parameters_default_values(wyckoff_coordinates);
  this->set_species(num, species, false);

  delete [] wyckoff; wyckoff = NULL;
  delete [] species; species = NULL;

  if(this->lattice_parameter_values == NULL || 
                    this->basis_parameter_values == NULL){  
    
    delete [] wyckoff_coordinates; wyckoff_coordinates = NULL;
    return;  
  }
  
  // ==========================================================
  int num_param = this->num_parameters;
  double *values = new double [num_param];
  string *name = new string [num_param];
  for(int i=0; i<num_param; i++){
    name[i] = this->parameters[i];
    values[i] = this->parameter_values[i];
  }
  this->set_lattice_from_parameter_values(num_param, name, values, false);
  
  delete [] values; values = NULL;
  delete [] name; name = NULL;

  if(this->lattice == NULL){
    delete [] wyckoff_coordinates; wyckoff_coordinates = NULL;
    return;
  }

  this->generate_name_and_wyckoff_orders(); 
  this->generate_species_permutations(); 

  // ==========================================================
  // get new mapped wyckoff and corresponding coordintes
  
  // get mapping from old wyckoff-sites to new wyckoff-sites
  double rotation[9], origin_shift[3];
  string sorder1, sorder2;
  sorder1 = ""; sorder2 = "";
  for(int i=0; i<this->num_wyckoff; i++){
    sorder1 += this->wyckoff[i] + " ";
    sorder2 += this->wyckoff_orders[
                  this->num_wyckoff * this->desired_wyckoff_order_id + i] + " ";
  }
  sorder1 = sorder1.substr(0, sorder1.length() - 1);
  sorder2 = sorder2.substr(0, sorder2.length() - 1);
  if( !if_wyckoff_order_related_through_symmetry(this->num_wyckoff,
                                                sorder2, sorder1, true,
                                                rotation, origin_shift) ){ 
   
    
    cout<<"Oops! not able to map to new Wyckoff-name!"<<'\n';
    cout<<"SpaceGroupNumber: "<<this->spaceGroupNumber<<'\t';
    cout<<"Wyckoff_orders: "<<sorder2<<'\t'<<sorder1<<'\n'; 
    cout<<" FATAL ERROR! EXITING!"<<'\n';
    exit(0);
  }

  // get new wyckoff coordinates
  for(int i=0; i<this->num_wyckoff; i++){
    for(int d=0;d<3;d++) temp_coord[d] = 0.0;
    for(int d1=0;d1<3;d1++){
      for(int d2=0;d2<3;d2++){
        temp_coord[d1] += rotation[3*d1 + d2] * wyckoff_coordinates[3*i + d2];
      }
    }
    for(int d=0;d<3;d++) wyckoff_coordinates[3*i+d] = temp_coord[d];
    for(int d=0;d<3;d++) wyckoff_coordinates[3*i+d] += origin_shift[d];
    
  }

  // copy new wyckoffs
  for(int i=0; i<this->num_wyckoff; i++){
    this->wyckoff[i] = this->wyckoff_orders[
                        this->num_wyckoff * this->desired_wyckoff_order_id + i];
  }


  // get new basis parameters and values
  this->calculate_basis_parameters_default_values(wyckoff_coordinates);
  

  // copy new basis parameters
  for(int i=0; i<this->num_basis_parameters; i++){
    this->parameters[this->num_lattice_parameters + i] = 
                                                    this->basis_parameters[i];
    this->parameter_values[this->num_lattice_parameters + i] = 
                                              this->basis_parameter_values[i];
  }
  delete [] wyckoff_coordinates; wyckoff_coordinates = NULL;
  // ==========================================================
 

  return;   
}
    
    
// ---------------------------------------------------------------------------//
string SYSTEM_BULK::get_symmetry_symbol_of_wyckoff_site(string site){

  for(int j=0; j<this->wyckoff_entry->num; j++){
    if(this->wyckoff_entry->sites[j].symbol == site){
      return this->wyckoff_entry->sites[j].symmetry;  
    }
  }
  
  return "";

}

// ---------------------------------------------------------------------------//
// set parameter values
void SYSTEM_BULK::set_parameter_values(int num, string *name, double *value){
  this->set_lattice_from_parameter_values(num, name, value);
  this->set_structure_from_file(this->structure_file_content, false);
}


// ---------------------------------------------------------------------------//
// set parameter values
void SYSTEM_BULK::set_lattice_from_parameter_values(int num, string *name, 
                                                    double *value, bool reset){
  
  if(reset) this->reset_parameter_values();
  
  if(this->parameter_values != NULL){
    delete [] this->parameter_values;
    this->parameter_values = NULL; 
  }
  this->parameter_values = new double [this->num_parameters];
  
  if(this->basis_parameter_values != NULL){
    delete [] this->basis_parameter_values;
    this->basis_parameter_values = NULL; 
  }
  this->basis_parameter_values = new double [this->num_basis_parameters];
  
  if(this->lattice_parameter_values != NULL){
    delete [] this->lattice_parameter_values;
    this->lattice_parameter_values = NULL; 
  }
  this->lattice_parameter_values = new double [this->num_lattice_parameters];

  bool if_found;
  
  // store lattice parameters
  for(int i=0; i<this->num_lattice_parameters; i++){
    if_found = false;
    for(int j=0; j<num; j++){
      if(this->lattice_parameters[i] == name[j]){
        this->lattice_parameter_values[i] = value[j];
        if_found = true;
        break;
      }
    }
    if(!if_found){
      cout<<"Not able to find value for parameter: "
                                        <<this->lattice_parameters[i]<<'\n';
      return;
    }
  }

  // store basis parameters
  for(int i=0; i<this->num_basis_parameters; i++){
    if_found = false;
    for(int j=0; j<num; j++){
      if(this->basis_parameters[i] == name[j]){
        this->basis_parameter_values[i] = value[j];
        if_found = true;
        break;
      }
    }
    if(!if_found){
      cout<<"Not able to find value for parameter: "
                                        <<this->basis_parameters[i]<<'\n';
      return;
    }
  }

  
  // combine lattice/basis parameters
  for(int i=0;i<this->num_lattice_parameters; i++){
    this->parameter_values[i] = this->lattice_parameter_values[i];
  }
  for(int i=0;i<this->num_basis_parameters; i++){
    this->parameter_values[this->num_lattice_parameters + i] = 
                                            this->basis_parameter_values[i];
  }

  this->calculate_lattice();
  
  LATTICE *temp_lattice = NULL;
  this->calculate_std_or_primitive_lattice(&temp_lattice, -1);  
  if(this->lattice != NULL){
    delete this->lattice;
    this->lattice = NULL;
  }
  if(temp_lattice == NULL){
    this->lattice = NULL;
    return;
  }
  this->lattice = temp_lattice;
  
  if(!reset){
    this->calculate_std_or_primitive_lattice(&this->std_lattice, -1);  
    this->calculate_std_or_primitive_lattice(&this->primitive_lattice, 1);  
  }



}


// ---------------------------------------------------------------------------//
// calculate default parameters used in structure 
void SYSTEM_BULK::calculate_parameters_default_values(
                                                  double *wyckoff_coordinates){
  // this function is called only when structure is read. And in this case, we
  // will have symmetry_dataset set

  if(this->symmetry_dataset == NULL){
    cout<<"Structure not read and as such symmetry dataset not set!";
    return;
  }
  
  this->calculate_lattice_parameters_default_values();
  if(this->lattice_parameter_values == NULL){
    cout<<"Not able to extract lattice parameter values!"<<'\n';
    return;
  }

  this->calculate_basis_parameters_default_values(wyckoff_coordinates);
  if(this->basis_parameter_values == NULL){
    cout<<"Not able to extract basis parameter values!"<<'\n';
    return;
  }

  // copy values now
  if(this->parameter_values != NULL){
    delete [] this->parameter_values;
    this->parameter_values = NULL;
  }
  
  this->parameter_values = new double [this->num_parameters];
  for(int i=0; i<this->num_lattice_parameters; i++)
    this->parameter_values[i] = this->lattice_parameter_values[i];
  
  for(int i=0; i<this->num_basis_parameters; i++){
    this->parameter_values[this->num_lattice_parameters + i] = 
                                        this->basis_parameter_values[i];
    this->parameters[this->num_lattice_parameters + i] = 
                                        this->basis_parameters[i];
  }

}



// ---------------------------------------------------------------------------//
// default lattice parameters from stored lattice
void SYSTEM_BULK::calculate_basis_parameters_default_values(
                                            double *wyckoff_coordinates){
  
  if(this->basis_parameter_values != NULL){
    delete [] this->basis_parameter_values;
    this->basis_parameter_values = NULL;
  }
  this->basis_parameter_values = new double [this->num_basis_parameters];
  
  if(this->basis_parameters != NULL){
    delete [] this->basis_parameters;
    this->basis_parameters = NULL;
  }
  this->basis_parameters = new string [this->num_basis_parameters];

  // we will try to match with continuously decreasing tolerance
  double A[9];
  double C[3];
  double X[3];
  double temp_tol = this->tolerance;
  bool satisfied, coordinate_found, if_solution_found;
  double origin[3];
  double max_factor;
  struct_wyckoff_site *wyckoff_site = NULL;
  int temp_num_basis_parameters;
  char coord[] = {'x', 'y', 'z'};

  temp_tol *= 1e-1;
  while(temp_tol < 1e-2){
    temp_tol *= 1e+1; 
    satisfied = true; 
    temp_num_basis_parameters = 0;

    for(int i=0; i<this->num_wyckoff; i++){
      coordinate_found = false;

      // find wyckoff site first
      for(int j=0; j<this->wyckoff_entry->num; j++){
        if(this->wyckoff_entry->sites[j].symbol == this->wyckoff[i]){
          wyckoff_site = &(this->wyckoff_entry->sites[j]);
          break;
        }
      }

      // loop over all sites
      for(int e=0; e<wyckoff_site->num; e++){

        // max factor to go for
        max_factor = 1.0;
        for(int c=0; c<3; c++){
          for(int ic=0; ic<3; ic++){
            max_factor = 
              (max_factor > abs(wyckoff_site->coordinates[12*e + 4*c  + ic])) ? 
              (max_factor):
              (abs(wyckoff_site->coordinates[12*e + 4*c + ic]));
          }
        }

        // unknown parameters for this site
        set<int> set_unknown;
        set<int>::iterator it_unknown;
        for(int c=0; c<3;c++){
          for(int ic=0; ic<3; ic++){
            if(abs(wyckoff_site->coordinates[12*e + 4*c + ic]) > 
                                                              this->tolerance)
              set_unknown.insert(ic);
          }
        }
       
        // all choices of origins
        for(int cc=0; cc<this->crystal_coordinate_entry->num; cc++){  
          for(int d=0;d<3;d++) 
            origin[d] = this->crystal_coordinate_entry->coordinates[3*cc+d];

      
       
          //------ all factors
          for(int f=0; f < (round(max_factor) + 1); f++){ 
          
            for(int c=0; c<3; c++){
              for(int ic=0; ic<3; ic++){
                A[3*c+ic] = wyckoff_site->coordinates[12*e + 4*c + ic];
              }
              C[c] = -wyckoff_site->coordinates[12*e + 4*c + 3];
              C[c] += wyckoff_coordinates[3*i + c];
              C[c] -= origin[c];
              C[c] += f;
            }
     
            solve_linear_system_of_reduced_rank_with_modulii_one(A, C, X,
                                                  &if_solution_found);
           
            if(if_solution_found){
            
              for(int c=0;c<3;c++){
                if(X[c] < -temp_tol) X[c] += 1.0;
              }
              
              // store the values
              for(it_unknown = set_unknown.begin(); it_unknown !=
                              set_unknown.end(); ++it_unknown){

                this->basis_parameter_values[temp_num_basis_parameters] = 
                        X[*it_unknown];
                this->basis_parameters[temp_num_basis_parameters] = 
                        coord[*it_unknown] + this->wyckoff[i] + int_to_str(i);
                temp_num_basis_parameters++;
  
              }
                
              coordinate_found = true;
              break;
            }

          }  
          //------


          if(coordinate_found) break;
        }
        if(coordinate_found) break;
      }
      
      if(coordinate_found == false){
        satisfied = false;
        break;
      }

    }

    if(satisfied) break;
  }

 
  if(!satisfied){
    cout<<"Not able to extract parameter values!"<<'\n';
    
    if(this->basis_parameter_values != NULL){
      delete [] this->basis_parameter_values;
      this->basis_parameter_values = NULL;
    }
    
    if(this->basis_parameters != NULL){
      delete [] this->basis_parameters;
      this->basis_parameters = NULL;
    }
    
    return;
  }
 
  // make sure all basis parameetrs are between 0 and 1
  double dtemp;
  for(int i=0; i<this->num_basis_parameters; i++){
    dtemp = this->basis_parameter_values[i];
    dtemp = dtemp - round(dtemp);
    if(dtemp < -this->tolerance) dtemp += 1.0;
    this->basis_parameter_values[i] = dtemp;    
  }
  
}



// ---------------------------------------------------------------------------//
// default lattice parameters from stored lattice
void SYSTEM_BULK::calculate_lattice_parameters_default_values(){
 
  // get cell parameters from lattice vectors first
  double cellpar[6];
  calculate_cellpar_from_alat(this->lattice->alat, cellpar, 1);


  if(this->lattice_parameter_values != NULL){
    delete [] this->lattice_parameter_values;
    this->lattice_parameter_values = NULL;
  }
  this->lattice_parameter_values = new double [this->num_lattice_parameters];

  if(this->spaceGroupNumber <= 2){
    for(int i=0; i<6; i++) this->lattice_parameter_values[i] = cellpar[i];
  }

  if(this->spaceGroupNumber >= 3 && this->spaceGroupNumber <= 15 ){
    for(int i=0; i<3; i++) this->lattice_parameter_values[i] = cellpar[i];
    this->lattice_parameter_values[3] = cellpar[4];
  }
  
  if(this->spaceGroupNumber >= 16 && this->spaceGroupNumber <= 74 ){
    for(int i=0; i<3; i++) this->lattice_parameter_values[i] = cellpar[i];
  }
  
  if(this->spaceGroupNumber >= 75 && this->spaceGroupNumber <= 142 ){
    this->lattice_parameter_values[0] = cellpar[0];
    this->lattice_parameter_values[1] = cellpar[2];
  }
 
  if(this->spaceGroupNumber >= 143 && this->spaceGroupNumber <= 167 ){
    
    //check if rhomboherdon or hexagonal
    double max_rhom, max_hex, temp;
    max_rhom = 0.0;
    max_hex = 0.0;
    
    temp = abs(cellpar[1] -1.0);
    max_rhom = (max_rhom > temp)?max_rhom:temp;
    temp = abs(cellpar[2] -1.0);
    max_rhom = (max_rhom > temp)?max_rhom:temp;
    temp = abs((cellpar[4]-cellpar[3])/cellpar[3]);
    max_rhom = (max_rhom > temp)?max_rhom:temp;
    temp = abs((cellpar[5]-cellpar[3])/cellpar[3]);
    max_rhom = (max_rhom > temp)?max_rhom:temp;
    
    temp = abs(cellpar[1] -1.0);
    max_hex = (max_hex > temp)?max_hex:temp;
    temp = abs((cellpar[3] - 90.)/90.);
    max_hex = (max_hex > temp)?max_hex:temp;
    temp = abs((cellpar[4] - 90.)/90.);
    max_hex = (max_hex > temp)?max_hex:temp;
    temp = abs((cellpar[5] - 120.)/120.);
    max_hex = (max_hex > temp)?max_hex:temp;

    if(max_rhom > max_hex)
      this->trigonal_flag = "hexagonal";
    else
      this->trigonal_flag = "rhombohedron";  
    
    // if hexagonal we are good and we simply need to add parameters
    if(this->trigonal_flag == "hexagonal"){
      this->lattice_parameter_values[0] = cellpar[0];
      this->lattice_parameter_values[1] = cellpar[2];

      // reset the crystal_coordinate_entry now
      if(this->crystal_coordinate_entry->symbol == 'R'){  
        for(int i=0; i<7; i++){
          if(this->crystal_coordinates[i].symbol == 'H'){
            this->crystal_coordinate_entry = &this->crystal_coordinates[i];
          }
        }
      }

    }

    if(this->trigonal_flag == "rhombohedron"){
      this->lattice_parameters[1] = "alpha";
      this->parameters[1] = "alpha";
      this->lattice_parameter_values[0] = cellpar[0];
      this->lattice_parameter_values[1] = cellpar[3];
    }
    
  }

  if(this->spaceGroupNumber >= 168 && this->spaceGroupNumber <= 194 ){
    this->lattice_parameter_values[0] = cellpar[0];
    this->lattice_parameter_values[1] = cellpar[2];
  }

  if(this->spaceGroupNumber >= 195 && this->spaceGroupNumber <= 230 ){
    this->lattice_parameter_values[0] = cellpar[0];
  }

}


// ---------------------------------------------------------------------------//
// get symmetry dataset
SpglibDataset* SYSTEM_BULK::calculate_symmetry_dataset(LATTICE *lattice){
 
  if(lattice == NULL){
    cout<<"Lattice not set!"<<'\n';
    return NULL;
  }
  
  
  // Get spglib symmetry dataset first
  double a[3][3];
  double (*position)[3] = new double [lattice->natom][3];
  int *types;
  types = new int [lattice->natom];

  for(int j=0;j<3;j++){
    for(int d=0;d<3;d++){
      a[d][j] = lattice->alat[3*j+d];
    }
  }
  for(int i=0;i<lattice->natom;i++){
    for(int d=0;d<3;d++){
      position[i][d] = lattice->basis[i].direct[d];
    }
    map_atomic_symbol_id(&types[i], &(lattice->basis[i].symbol), 1);
  }
  
  SpglibDataset *symmetry_dataset = spg_get_dataset(a, position, types, 
                                          lattice->natom, this->tolerance);

  if(symmetry_dataset == NULL){
    double temp_tolerance = this->tolerance;
    while(symmetry_dataset == NULL && temp_tolerance > 1e-8){
      temp_tolerance *= 1e-1;
      symmetry_dataset = spg_get_dataset(a, position, types, 
                                        lattice->natom, this->tolerance);
    };
  }
  delete [] position; position = NULL;
  delete [] types;  types = NULL;
  if(symmetry_dataset == NULL){
    cout<<"Cannot find symmetry dataset!"<<'\n';
    return NULL; 
  }

  return symmetry_dataset;
    
}


// ---------------------------------------------------------------------------//
// set species
void SYSTEM_BULK::set_species(int num, string *species, bool reset){ 

  if(reset) this->reset_species();
   
  if(this->wyckoff == NULL){
    cout<<"Wyckoff not set!"<<'\n';
    return;
  }
  
  if(this->species != NULL){ 
    delete [] this->species; 
    this->species = NULL;
  }
  this->num_species = num;
  this->species = new string [this->num_species];
  for(int i=0; i<num; i++) this->species[i] = species[i];
  
}



// ---------------------------------------------------------------------------//
// set wyckoff
void SYSTEM_BULK::set_wyckoff(int num, string *wyckoff, bool reset){ 

  if(reset) this->reset_wyckoff();

  if(this->spaceGroupNumber == 0){
    cout<<"spaceGroup is not set!"<<'\n';
    return;
  }
  
  if(this->wyckoff != NULL){
    delete [] this->wyckoff; 
    this->wyckoff = NULL;
  }
  this->num_wyckoff = num;
  this->wyckoff = new string [this->num_wyckoff];
  for(int i=0; i<num; i++) this->wyckoff[i] = wyckoff[i];

  this->calculate_parameters(); 
  if(this->parameters == NULL) return;

  
}



// ---------------------------------------------------------------------------//
// function to find unknown parameters in spaceGroup with given Wyckoff
void SYSTEM_BULK::calculate_parameters(){

  if(this->spaceGroupNumber == 0){
    cout<<"spaceGroup is not set!"<<'\n';
    return;
  }
  if(this->wyckoff == NULL){
    cout<<"Wyckoff not set!"<<'\n';
    return;
  }
  
  this->trigonal_flag = "hexagonal";
  this->calculate_lattice_parameters();
  this->calculate_basis_parameters();

  if(this->parameters != NULL){
    delete [] this->parameters;
    this->parameters = NULL;
  }
  this->num_parameters = 
                this->num_lattice_parameters + this->num_basis_parameters; 
  this->parameters = new string [this->num_parameters];


  for(int i=0; i<this->num_lattice_parameters; i++){
    this->parameters[i] = this->lattice_parameters[i];
  }
  for(int i=0; i<this->num_basis_parameters; i++){
    this->parameters[this->num_lattice_parameters + i] = 
                                          this->basis_parameters[i];
  }

  return;
}


// ---------------------------------------------------------------------------//
// get basis parameters, depending on the spaceGroup and wyckoff
void SYSTEM_BULK::calculate_basis_parameters(){

  if(this->spaceGroupNumber == 0){
    cout<<"spaceGroup is not set!"<<'\n';
    return;
  }
  if(this->wyckoff == NULL){
    cout<<"Wyckoff not set!"<<'\n';
    return;
  }
  
  int temp_num = 0;
  string *temp_parameters = new string [3*this->num_wyckoff];
  char coord[] = {'x', 'y', 'z'};

  for(int si=0; si<this->num_wyckoff; si++){  
    for(int sj=0; sj<this->wyckoff_entry->num; sj++){
      if(this->wyckoff[si] == this->wyckoff_entry->sites[sj].symbol){
            
        struct_wyckoff_site *wyckoff_site = &this->wyckoff_entry->sites[sj];

        set<string> temp;
        set<string>::iterator it;
        for(int s=0; s<wyckoff_site->num; s++){
          for(int c=0; c<3;c++){
            for(int ic=0; ic<3; ic++){
              if(abs(wyckoff_site->coordinates[12*s + 4*c + ic]) > 
                    this->tolerance)
                temp.insert(coord[ic] + wyckoff_site->symbol + 
                                                        int_to_str(si)); 
            }
          }
        }
            
        for(it=temp.begin(); it!=temp.end(); ++it){
          temp_parameters[temp_num] = *it;
          temp_num++;
        }
        break;
    
      }
    }
        
  }

  if(this->basis_parameters != NULL){
    delete [] this->basis_parameters;
    this->basis_parameters = NULL;
  }
  this->num_basis_parameters = temp_num;
  this->basis_parameters = new string [this->num_basis_parameters];
  for(int i=0; i<temp_num; i++){
    this->basis_parameters[i] = temp_parameters[i];
  }

  if(this->num_wyckoff > 0){
    delete [] temp_parameters;
    temp_parameters = NULL;
  }
  return;

}
  

// ---------------------------------------------------------------------------//
// get lattice parameters, depending on the spaceGroup
void SYSTEM_BULK::calculate_lattice_parameters(){

  if(this->spaceGroupNumber == 0){
    cout<<"spaceGroup is not set!"<<'\n';
    return;
  }
  
  if(this->lattice_parameters != NULL){
    delete [] this->lattice_parameters;
    this->lattice_parameters = NULL;
  }

  // convert flag to lower case
  for(unsigned int i=0; i<this->trigonal_flag.length(); i++){
    this->trigonal_flag[i] = tolower(this->trigonal_flag[i]);
  }


  if(this->spaceGroupNumber <= 2){
    this->num_lattice_parameters = 6;
    this->lattice_parameters = new string [this->num_lattice_parameters];
    this->lattice_parameters[0] = "a";
    this->lattice_parameters[1] = "b/a";
    this->lattice_parameters[2] = "c/a";
    this->lattice_parameters[3] = "alpha";
    this->lattice_parameters[4] = "beta";
    this->lattice_parameters[5] = "gamma";
  }
  
  if(this->spaceGroupNumber >= 3 && this->spaceGroupNumber <= 15){
    this->num_lattice_parameters = 4;
    this->lattice_parameters = new string [this->num_lattice_parameters];
    this->lattice_parameters[0] = "a";
    this->lattice_parameters[1] = "b/a";
    this->lattice_parameters[2] = "c/a";
    this->lattice_parameters[3] = "beta";
  }

  if(this->spaceGroupNumber >= 16 && this->spaceGroupNumber <= 74){
    this->num_lattice_parameters = 3;
    this->lattice_parameters = new string [this->num_lattice_parameters];
    this->lattice_parameters[0] = "a";
    this->lattice_parameters[1] = "b/a";
    this->lattice_parameters[2] = "c/a";
  }
  
  if(this->spaceGroupNumber >= 75 && this->spaceGroupNumber <= 142){
    this->num_lattice_parameters = 2;
    this->lattice_parameters = new string [this->num_lattice_parameters];
    this->lattice_parameters[0] = "a";
    this->lattice_parameters[1] = "b/a";
  }
  
  if(this->spaceGroupNumber >= 143 && this->spaceGroupNumber <= 167){
    int locate;
    if((locate = this->trigonal_flag.find("hex")) >= 0){
      this->num_lattice_parameters = 2;
      this->lattice_parameters = new string [this->num_lattice_parameters];
      this->lattice_parameters[0] = "a";
      this->lattice_parameters[1] = "c/a";
    }else{
      this->num_lattice_parameters = 2;
      this->lattice_parameters = new string [this->num_lattice_parameters];
      this->lattice_parameters[0] = "a";
      this->lattice_parameters[1] = "alpha";
    }
  }

  if(this->spaceGroupNumber >= 168 && this->spaceGroupNumber <= 194){
    this->num_lattice_parameters = 2;
    this->lattice_parameters = new string [this->num_lattice_parameters];
    this->lattice_parameters[0] = "a";
    this->lattice_parameters[1] = "c/a";
  }

  if(this->spaceGroupNumber >= 195 && this->spaceGroupNumber <= 230){
    this->num_lattice_parameters = 1;
    this->lattice_parameters = new string [this->num_lattice_parameters];
    this->lattice_parameters[0] = "a";
  }

}


// ---------------------------------------------------------------------------//
// get Wyckoff of a given spaceGroupNumber
void SYSTEM_BULK::get_wyckoff_list(int *num, int
                              **multiplicity, int **if_repeatable, 
                              string **site_symbol){

  if(this->wyckoff_entry == NULL){
    cout<<"spaceGroup is not set!"<<'\n';
    *num = 0;
    return;
  }

  *num = this->wyckoff_entry->num;
  *site_symbol = new string [*num];
  *multiplicity = new int [*num];
  *if_repeatable = new int [*num];
  bool if_repeat;
  for(int j=0; j<*num; j++){
    (*site_symbol)[j] = this->wyckoff_entry->sites[j].symbol;
    (*multiplicity)[j] = this->wyckoff_entry->sites[j].multiplicity;
    if_repeat = false;
    for(int c=0; c<3; c++){
      for(int ic=0; ic<3; ic++){
        if(abs(this->wyckoff_entry->sites[j].coordinates[12*0 + 4*c + ic]) > 
                                                            this->tolerance){
          if_repeat = true;
          break;
        }
      }
      if(if_repeat) break;
    }
    if(if_repeat) (*if_repeatable)[j] = 1;
    else (*if_repeatable)[j] = -1;
  }
}


// ---------------------------------------------------------------------------//
// return what all species permutations are unique
void SYSTEM_BULK::generate_species_permutations(){
  
  if(this->species_permutations != NULL){
    delete [] this->species_permutations;
    this->species_permutations = NULL;
    this->num_species_permutations = 0;
  }
  
  if(this->wyckoff_orders == NULL){
    cout<<"Wyckoff-orders should be set before species_permutations!"<<'\n';
    return;
  }


  // initialize memory
  int ntype;
  struct_specie_wyckoff **specie_wyckoff = 
                        new struct_specie_wyckoff* [this->num_wyckoff_orders];
  for(int i=0; i<this->num_wyckoff_orders; i++) specie_wyckoff[i] = NULL;


  // get specie_wyckoff of all wyckoff_orders
  for(int i=0; i<this->num_wyckoff_orders; i++){
    get_specie_wyckoff_from_wyckoff_order(
                                  &this->wyckoff_orders[this->num_wyckoff*i], 
                                  &ntype, 
                                  &specie_wyckoff[i]);
  
  }


  // find the one where wyckoffs are same as that from desired_wyckoff_order_id
  // already done orders
  bool if_valid, if_found;
  int num_orders_done = 0;
  string *orders_done = new string [ntype * this->num_wyckoff_orders];
  bool *temp_wyckoff_done = new bool [ntype];
  for(int i=0; i<this->num_wyckoff_orders; i++){
    
    // check if a valid case
    if_valid = true;
    for(int j=0; j<ntype; j++) temp_wyckoff_done[j]= false;
    for(int j=0; j<ntype; j++){
      
      if_found = false;
      for(int k=0; k<ntype; k++){
        if(temp_wyckoff_done[k]) continue;
        if(specie_wyckoff[i][j].wyckoff_string == 
              specie_wyckoff[this->desired_wyckoff_order_id][k].wyckoff_string){
          if_found = true;
          temp_wyckoff_done[k] = true;
          break;
        }
      } //over k

      if(!if_found){
        if_valid = false;
        break;
      }

    } // over j
    
    if(!if_valid)
      continue;
  
  
    // store it now 
    for(int j=0; j<ntype; j++) temp_wyckoff_done[j]= false;
    for(int j=0; j<ntype; j++){
      for(int k=0; k<ntype; k++){
        if(temp_wyckoff_done[k]) continue;
        if(specie_wyckoff[this->desired_wyckoff_order_id][k].wyckoff_string ==
                  specie_wyckoff[i][j].wyckoff_string){
          orders_done[ntype*num_orders_done + j] = specie_wyckoff[i][k].element;
          temp_wyckoff_done[k] = true;
          break;
        }
      }
    }
    num_orders_done++;    
    
  } //over i
  delete [] temp_wyckoff_done;
  

  // get map from original to all orders_done
  int *map_to_done = new int [num_orders_done * ntype];
  for(int i=0; i<num_orders_done*ntype; i++) map_to_done[i] = -1;
  string *given_specie_order = new string [ntype];
  for(int j=0; j<ntype; j++){
    given_specie_order[j] = 
                    specie_wyckoff[this->desired_wyckoff_order_id][j].element;
  }
  for(int i=0; i<num_orders_done; i++){
    for(int j=0; j<ntype; j++){
      for(int k=0; k<ntype; k++){
        if(given_specie_order[j] == orders_done[ntype*i+k]){
          map_to_done[ntype*i + j] = k;
          break; 
        }
      }
      if(map_to_done[ntype*i + j] == -1){
        cout<<"Fatal Error!. Failed in mapping specie-permutations! Exiting";
        cout<<'\n';
        exit(0);
      }
    }
  }
  delete [] given_specie_order;
  delete [] orders_done;


  // get all permutations possible for species
  string *temp_permu = new string [ntype];
  for(int i=0; i<ntype; i++) temp_permu[i] = 
                     specie_wyckoff[this->desired_wyckoff_order_id][i].element;
  int npermu = 1;
  for(int i=0; i<ntype; i++) npermu *= (i+1); 
  string *total_permu = new string [ntype * npermu];
  int num_total_permu = 0;
  do{
    for(int i=0; i<ntype; i++) 
      total_permu[ntype*num_total_permu + i] = temp_permu[i];
    num_total_permu++;  
  }while(next_permutation(temp_permu, temp_permu + ntype));
  delete [] temp_permu; temp_permu = NULL;

  // filter out what is already done due to wyckoff_orders
  int num_undone_permu = 0;
  int *undone_permu = new int [num_total_permu];
  bool if_done;
  for(int i=0; i<num_total_permu; i++){
   
    if_done = false;
    for(int j=0; j<num_undone_permu; j++){
      
      for(int k=0; k<num_orders_done; k++){
        

        if_found = true;
        for(int l=0; l<ntype; l++){
          if(total_permu[ntype*i + l] != 
                total_permu[ntype*undone_permu[j] + map_to_done[ntype*k + l]]){
            if_found = false;
            break;
          }
        }
        if(if_found){
          if_done = true;
          break;
        }
      } // over k


    }// over j

    if(if_done)
      continue;
    
    undone_permu[num_undone_permu] = i;
    num_undone_permu++;  
    
  } //over i

  // store the final list; 
  this->num_species_permutations = num_undone_permu;
  this->species_permutations = new string [this->num_species_permutations];

  //store other orders
  for(int i=0; i<num_undone_permu; i++){
    this->species_permutations[i] = "";
    for(int j=0; j<ntype; j++){
      this->species_permutations[i] += 
                            total_permu[ntype*undone_permu[i] + j] + "_";
    }
    this->species_permutations[i] = 
                    this->species_permutations[i].substr(0, 
                              this->species_permutations[i].length() - 1); 
  }


  // free the memory
  delete [] total_permu;
  delete [] map_to_done;
  delete [] undone_permu;
  
  for(int i=0; i<this->num_wyckoff_orders; i++){
    if(specie_wyckoff[i] != NULL) delete [] specie_wyckoff[i];
  }
  delete [] specie_wyckoff;
   
}


// ---------------------------------------------------------------------------//
// get specie_wyckoff datastructure from given wyckoff_order
void SYSTEM_BULK::get_specie_wyckoff_from_wyckoff_order(
                                      string *wyckoff_order, 
                                      int *ntype,
                                      struct_specie_wyckoff **specie_wyckoff){
  
  // initialize memory
  *specie_wyckoff = new struct_specie_wyckoff[this->num_wyckoff];
  for(int i=0; i<this->num_wyckoff; i++)
                  (*specie_wyckoff)[i].wyckoff = new string [this->num_wyckoff];
  
  
  // get wyckoff of eact atom
  *ntype = 0;
  bool if_different;
  for(int i=0; i<this->num_wyckoff; i++){
    if_different = true;
    for(int j=0; j<(*ntype); j++){
      if(this->species[i] == (*specie_wyckoff)[j].element){
        (*specie_wyckoff)[j].wyckoff[(*specie_wyckoff)[j].num_wyckoff] = 
                                                            wyckoff_order[i];
        (*specie_wyckoff)[j].num_wyckoff++;
        if_different = false;
        break;
      }
    }
      
    if(if_different){
      (*specie_wyckoff)[(*ntype)].wyckoff[0] = wyckoff_order[i];
      (*specie_wyckoff)[(*ntype)].element = this->species[i];
      (*specie_wyckoff)[(*ntype)].num_wyckoff = 1;
      (*ntype)++;
    }
  }
 
  // get count of each atom now
  for(int i=0; i<(*ntype); i++){
    for(int j=0; j<(*specie_wyckoff)[i].num_wyckoff; j++){
      for(int k=0; k<this->wyckoff_entry->num; k++){
        if(this->wyckoff_entry->sites[k].symbol == 
                                          (*specie_wyckoff)[i].wyckoff[j]){
          (*specie_wyckoff)[i].num_atom += 
                                this->wyckoff_entry->sites[k].multiplicity;
          break;
        }
      }
    }
  }

  //......... now get wyckoff string of each specie
  for(int i=0; i<(*ntype); i++){
    multiset<string> multiset_temp;
    set<string> set_temp;
    set<string>::iterator it;
    string stemp;
    int itemp;
    for(int j=0; j<(*specie_wyckoff)[i].num_wyckoff; j++){
      multiset_temp.insert((*specie_wyckoff)[i].wyckoff[j]);
      set_temp.insert((*specie_wyckoff)[i].wyckoff[j]);
    }    
    (*specie_wyckoff)[i].wyckoff_string = "";
    for(it=set_temp.begin(); it != set_temp.end(); ++it){
      stemp = *it;
      itemp = multiset_temp.count(stemp); 
      (*specie_wyckoff)[i].wyckoff_string += stemp;
      if(itemp > 1) (*specie_wyckoff)[i].wyckoff_string += int_to_str(itemp);
    }
  }


}



// ---------------------------------------------------------------------------//
// get wyckoff string from wyckoff identity and done wyckoffs
string SYSTEM_BULK::get_name_string_from_given_wyckoff_order(
                                                        string *wyckoff_order){
  
  string to_return = "";

  int ntype;
  struct_specie_wyckoff *specie_wyckoff = NULL;
  get_specie_wyckoff_from_wyckoff_order(wyckoff_order, &ntype, &specie_wyckoff);
  
  // .................... now sort by increasing number of atoms
  int *specie_order = new int [this->num_wyckoff];
  for(int i=0; i<ntype; i++) specie_order[i] = i;
  int itemp;
  bool if_sorted;
  do{
    if_sorted = true;
    for(int i=0; i<ntype-1; i++){
      if(specie_wyckoff[specie_order[i]].num_atom > 
                    specie_wyckoff[specie_order[i+1]].num_atom){

        if_sorted = false;
        itemp = specie_order[i];
        specie_order[i] = specie_order[i+1];
        specie_order[i+1] = itemp;

      }
    }
  }while(if_sorted == false);

  
  // ..... now sort by wyckoff string if num_atoms are same
  do{
    if_sorted = true;
    for(int i=0; i<ntype-1; i++){
      if( (specie_wyckoff[specie_order[i]].num_atom == 
            specie_wyckoff[specie_order[i+1]].num_atom)  && 
              (specie_wyckoff[specie_order[i]].wyckoff_string.compare(
                specie_wyckoff[specie_order[i+1]].wyckoff_string) > 0) ){
        if_sorted = false;
        itemp = specie_order[i];
        specie_order[i] = specie_order[i+1];
        specie_order[i+1] = itemp;
      }
    }
  }while(if_sorted == false);
 

  // get gcd
  int gcd = specie_wyckoff[specie_order[0]].num_atom/ 
                                           this->crystal_coordinate_entry->num;
  for(int i=1; i<ntype; i++)
    gcd = calculate_gcd(gcd, specie_wyckoff[specie_order[i]].num_atom/ 
                                    this->crystal_coordinate_entry->num);

  
  // now just get the name
  string alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  string stoichiometry = "";
  string wyckoff_string = "";
  for(int i=0; i<ntype; i++){
    stoichiometry += alphabets[i%26];
    if( (specie_wyckoff[specie_order[i]].num_atom/
                      this->crystal_coordinate_entry->num) / gcd > 1)
      stoichiometry += int_to_str( (specie_wyckoff[specie_order[i]].num_atom / 
                                   this->crystal_coordinate_entry->num) / gcd);
    wyckoff_string += specie_wyckoff[specie_order[i]].wyckoff_string + "_";   
  }
   
  to_return = stoichiometry + "_" +  int_to_str(gcd) + 
                   "_" + wyckoff_string + int_to_str(spaceGroupNumber);

  if(specie_wyckoff != NULL) delete [] specie_wyckoff;  
  if(specie_order != NULL) delete [] specie_order;

  return to_return;

}



// ---------------------------------------------------------------------------//
// handle same identity wyckoff pairs
void SYSTEM_BULK::handle_wyckoff_of_same_identity(int *wyckoff_identity,
                                                  int identity,
                                                  int *num, 
                                                  string **wyckoff_list){

  
  // num_wyckoffs to deal with
  int wyckoff_to_deal = 0;
  for(int i=0; i<this->num_wyckoff; i++){
    if(identity == wyckoff_identity[i])
      wyckoff_to_deal++;
  }

  if(wyckoff_to_deal == 0){
    // nothing to do
    *num = 0;
    *wyckoff_list = NULL;
    return;
  }

  // site-symmetry and num_symmetric_sites 
  int num_symmetric_sites = 0;
  string site_symmetry;  
  string *symmetric_sites = NULL;
  for(int i=0; i<this->num_wyckoff; i++){
    if(identity != wyckoff_identity[i])
      continue;
    
    // get symmetry
    for(int j=0; j<this->wyckoff_entry->num; j++){
      if(this->wyckoff[i] == this->wyckoff_entry->sites[j].symbol){
        site_symmetry = this->wyckoff_entry->sites[j].symmetry;
        break; 
      }
    }
    
    // count symmetric sites
    num_symmetric_sites = 0;
    for(int j=0; j<this->wyckoff_entry->num; j++){
      if(site_symmetry == this->wyckoff_entry->sites[j].symmetry)
        num_symmetric_sites++;
    }
    
    // symmetric_sites
    symmetric_sites = new string [num_symmetric_sites];
    num_symmetric_sites = 0;
    for(int j=0; j<this->wyckoff_entry->num; j++){
      if(site_symmetry == this->wyckoff_entry->sites[j].symmetry){
        symmetric_sites[num_symmetric_sites] = 
                          this->wyckoff_entry->sites[j].symbol;
        num_symmetric_sites++;
      }  
    }
  
    break;
  }

  // variables 
  int num_current, num_prev, num_total;
  string *prev_list, *current_list, *total_list;
  prev_list = NULL;
  current_list = NULL;
  total_list = NULL;

  num_total = 0;
  num_prev = 0;


  // do first wyckoff
  string sorder1, sorder2;
  num_total = num_symmetric_sites;
  total_list = new string [this->num_wyckoff * num_total];
  num_total = 0;
  int first_identity = 0;
  for(int i=0; i<this->num_wyckoff; i++){
    if(identity != wyckoff_identity[i])
      continue;
   
    sorder1 = this->wyckoff[i];
  
    for(int j=0; j<num_symmetric_sites; j++){
      
      sorder2 = symmetric_sites[j];

      if( !if_wyckoff_order_related_through_symmetry(1, sorder1, sorder2) )
        continue;
      
      total_list[this->num_wyckoff * num_total + i] = symmetric_sites[j];
      num_total++;
    }
    
    first_identity = i;
    break;  
  }


  // now do remaining wyckoffs
  bool if_valid;
  for(int i=0; i<this->num_wyckoff; i++){
    if(identity != wyckoff_identity[i])
      continue;
    if(i == first_identity)
      continue;

    // re-initialize prev-list
    num_prev = num_total;
    prev_list = total_list;
    
    // re-initialize current-list
    num_current = 0;
    current_list = new string [this->num_wyckoff * num_total * 
                                                          num_symmetric_sites];
    
    for(int j=0; j<num_prev; j++){
      
      for(int k=0; k<num_symmetric_sites; k++){
  
        
        // check if a valid permu
        if_valid = true;
        for(int l=0; l<i; l++){
          if(identity != wyckoff_identity[l])
            continue;
          sorder1 = this->wyckoff[l] + " " + this->wyckoff[i];
          sorder2 = prev_list[this->num_wyckoff*j + l] + " " + 
                                                    symmetric_sites[k];  
          if_valid = if_symmetric_wyckoff_pairs(sorder1, sorder2);
          if(!if_valid)
            break;
        } // over l

        
        if(!if_valid)
          continue;
        
        // add to current list now
        for(int l=0; l<i; l++){
          if(identity != wyckoff_identity[l])
            continue;
            current_list[this->num_wyckoff*num_current + l] = 
                              prev_list[this->num_wyckoff * j + l];  
        }
        current_list[this->num_wyckoff*num_current + i] = symmetric_sites[k];
        num_current++;

      } //over k
    
    } // over j


    // allocate total list
    num_total = num_current;
    total_list = new string [this->num_wyckoff * num_total];
    for(int j=0; j<num_total; j++){
      for(int k=0; k<this->num_wyckoff; k++){
        total_list[this->num_wyckoff*j + k] = 
                                        current_list[this->num_wyckoff*j+k];
      }   
    }

    // delete lists
    if(prev_list != NULL) delete [] prev_list;
    if(current_list != NULL) delete [] current_list;

  }//over i

  *num = num_total;
  *wyckoff_list = total_list;  

  delete [] symmetric_sites;
}


// ---------------------------------------------------------------------------//
// generate all possible wyckoff combinations
void SYSTEM_BULK::generate_valid_wyckoff_permutations_list(
                                            int *wyckoff_identity, 
                                            int current_identity,
                                            int *num, string **wyckoff_list){

  if(current_identity == 0){
    *num = 0;
    *wyckoff_list = NULL;
    return;
  }
  
  // get next identity
  bool if_set = false;
  int next_identity = 1e+7;
  for(int i=0; i<this->num_wyckoff; i++){
    if(next_identity > wyckoff_identity[i] && 
                                  current_identity < wyckoff_identity[i]){
      next_identity = wyckoff_identity[i];
      if_set = true;
    }
  }
  if(!if_set) next_identity = 0;

  // handle current group
  int num_current;
  string *current_list = NULL;
  handle_wyckoff_of_same_identity(wyckoff_identity, current_identity,
                                             &num_current, &current_list);
  
  // get next groups
  int num_next;
  string *next_list = NULL;
  generate_valid_wyckoff_permutations_list(wyckoff_identity, next_identity, 
                                                    &num_next, &next_list);

  // merge current and next
  if(num_next == 0){
    *num = num_current;
    *wyckoff_list = current_list;
    if(next_list != NULL) delete [] next_list;
    return;
  }
  if(num_current == 0){
    *num = num_next;
    *wyckoff_list = next_list;
    if(current_list != NULL) delete [] current_list;
    return;
  }
  
 
  int num_total = num_current * num_next;
  string *total_list = NULL;
  total_list = new string [this->num_wyckoff * num_total];
  
  num_total = 0;
  int num_in_order;
  string sorder1, sorder2;
  for(int i=0; i<num_current; i++){
    for(int j=0; j<num_next; j++){
      
      sorder1 = ""; 
      sorder2 = "";
      num_in_order = 0;
      for(int k=0; k<this->num_wyckoff; k++){
        
        if(wyckoff_identity[k] < current_identity)
          continue;
        
        if(wyckoff_identity[k] == current_identity){
          sorder1 += this->wyckoff[k] + " ";
          sorder2 += current_list[this->num_wyckoff*i + k] + " "; 
          num_in_order++;
        }
        
        if(wyckoff_identity[k] > current_identity){
          sorder1 += this->wyckoff[k] + " ";
          sorder2 += next_list[this->num_wyckoff*j + k] + " ";
          num_in_order++;
        }

      }

      // copy the entry
      if( if_wyckoff_order_related_through_symmetry(num_in_order, sorder1,
                                                          sorder2) ){
        for(int k=0; k<this->num_wyckoff; k++){
          if(wyckoff_identity[k] < current_identity)
            total_list[this->num_wyckoff * num_total + k] = "";
          if(wyckoff_identity[k] == current_identity)
            total_list[this->num_wyckoff * num_total + k] = 
                                current_list[this->num_wyckoff * i + k];
          if(wyckoff_identity[k] > current_identity)
            total_list[this->num_wyckoff * num_total + k] = 
                                   next_list[this->num_wyckoff * j + k];
        }
        num_total++;
      }

    }//over j
  }// over i


  
  // now copy the final enteris
  *num = num_total;
  *wyckoff_list = new string [this->num_wyckoff * (*num)];
  for(int i=0; i<num_total; i++){
    for(int j=0; j<this->num_wyckoff; j++){
      (*wyckoff_list)[this->num_wyckoff*i+j] = 
                                        total_list[this->num_wyckoff*i+j];
    }
  }
 
  
  // free memory
  if(current_list != NULL) delete [] current_list;
  if(next_list != NULL) delete [] next_list;
  if(total_list != NULL) delete [] total_list;
  
  return;    
}


// ---------------------------------------------------------------------------//
// generate valid wyckoff_orders 
void SYSTEM_BULK::generate_name_and_wyckoff_orders(){
 
  if(this->wyckoff_entry == NULL){
    cout<<"spaceGroup not set!"<<'\n';
    return;
  }
  if(wyckoff == NULL){
    cout<<"Wyckoff not set!"<<'\n';
    return;
  }
  if(species == NULL){
    cout<<"Species not set!"<<'\n';
    return;
  }
  
  // get wyckoff identity
  int *wyckoff_identity = new int [this->num_wyckoff];
  get_wyckoff_identity(wyckoff_identity);

  
  // get min identity
  int current_identity = 1e+7;
  for(int i=0; i<this->num_wyckoff; i++){
    if(current_identity > wyckoff_identity[i])
      current_identity = wyckoff_identity[i];
  }
 
  int num;
  string *wyckoff_list;
  generate_valid_wyckoff_permutations_list(wyckoff_identity, current_identity, 
                                                        &num, &wyckoff_list);

  
  // store wyckoff orders
  this->num_wyckoff_orders = num;
  this->wyckoff_orders = new string [this->num_wyckoff * 
                                          this->num_wyckoff_orders]; 
  for(int i=0; i<this->num_wyckoff_orders; i++){
    for(int j=0; j<this->num_wyckoff; j++){
      this->wyckoff_orders[this->num_wyckoff*i + j] 
                                    = wyckoff_list[this->num_wyckoff*i + j]; 
    }
  }

  // now also get the desired order -id [the least alphabetically]
  string *names = new string [this->num_wyckoff_orders];
  for(int i=0; i<this->num_wyckoff_orders; i++){
    names[i] = get_name_string_from_given_wyckoff_order(
                                  &this->wyckoff_orders[this->num_wyckoff*i]);
  }

  this->desired_wyckoff_order_id = 0;
  for(int i=0; i<this->num_wyckoff_orders; i++){
    if(names[this->desired_wyckoff_order_id].compare(names[i]) > 0)
      this->desired_wyckoff_order_id = i;
  }
  
  this->name = names[this->desired_wyckoff_order_id]; 

  delete [] names;
  delete [] wyckoff_list;
  delete [] wyckoff_identity;
}



// ---------------------------------------------------------------------------//
// get identity of wyckoffs by considering num_species of each type and wyckoff
//  alphabatical order
void SYSTEM_BULK::get_wyckoff_identity(int *wyckoff_identity){
   
  // count number of atoms of each specie type first  
  multiset<string> multiset_temp_species;
  for(int i=0; i<this->num_species; i++){
    for(int j=0; j<this->wyckoff_entry->num; j++){
      if(this->wyckoff[i] == this->wyckoff_entry->sites[j].symbol){
        for(int k=0; k<this->wyckoff_entry->sites[j].multiplicity; k++){
          multiset_temp_species.insert(this->species[i]);
        }
        break;   
      }
    }
  }

  int *temp_species_count = new int [this->num_species];
  for(int i=0; i<this->num_species; i++){
    temp_species_count[i] = multiset_temp_species.count(this->species[i]);
  }
  multiset_temp_species.clear();
  
  // get alphabatically identity of all wyckoffs
  int *temp_wyckoff_identity = new int [this->num_wyckoff];
  for(int i=0; i<this->num_wyckoff; i++) temp_wyckoff_identity[i] = 1e+4;
  string temp_symmetry;
  int temp_id;
  for(int i=0; i<this->num_wyckoff; i++){
    for(int j=0; j<this->wyckoff_entry->num; j++){
      if(this->wyckoff[i] == this->wyckoff_entry->sites[j].symbol){
        temp_symmetry = this->wyckoff_entry->sites[j].symmetry;
        break;   
      }
    }
    for(int j=0; j<this->wyckoff_entry->num; j++){
      if(temp_symmetry == this->wyckoff_entry->sites[j].symmetry){
        temp_id = 0;
        for(unsigned int k=0; k<this->wyckoff_entry->sites[j].symbol.length(); 
                                                                      k++){
          temp_id += (100*k) + 
                  (int(this->wyckoff_entry->sites[j].symbol[0]) - 65);
        }
        if(temp_id < temp_wyckoff_identity[i])
          temp_wyckoff_identity[i] = temp_id;          
        
      }
    }
  }

  // now get final identity of all wyckoffs
  for(int i=0; i<this->num_wyckoff; i++){
    wyckoff_identity[i] = 100*temp_species_count[i] + temp_wyckoff_identity[i]; 
  }
  
  delete [] temp_wyckoff_identity;
  delete [] temp_species_count;
  
  return;
}


// ---------------------------------------------------------------------------//
// loop over all spacegroups and find symmetric wyckoff pairs
void SYSTEM_BULK::generate_symmetric_wyckoff_pairs(){
 
  this->symmetric_wyckoff_pairs = new struct_symmetric_wyckoff_pairs* 
                                                      [this->num_spaceGroups];

  if(read_symmetric_wyckoff_pairs()){
    return;
  }

  
  string symbol_i, symbol_j;
  string symmetry_i, symmetry_j;
  string symbol_ii, symbol_jj;
  string symmetry_ii, symmetry_jj;
  bool if_repeat_i, if_repeat_ii;
  
  string sorder1, sorder2;
  bool if_same;

  // loop over all spacegroups
  for(int sg=1; sg<=this->num_spaceGroups; sg++){
    this->symmetric_wyckoff_pairs[sg-1] = new struct_symmetric_wyckoff_pairs;

    this->set_spacegroup(sg);

    // loop over all i
    for(int i=0; i<this->wyckoff_entry->num; i++){
      symbol_i = this->wyckoff_entry->sites[i].symbol;
      symmetry_i = this->wyckoff_entry->sites[i].symmetry;     
      
      if_repeat_i = false;     
      for(int c=0; c<3; c++){
        for(int ic=0; ic<3; ic++){
          if(abs(this->wyckoff_entry->sites[i].coordinates[12*0 + 4*c + ic]) > 
                                                            this->tolerance){
            if_repeat_i = true;
            break;
          }
        }
        if(if_repeat_i) break;
      }
    
      // loop over all j
      for(int j=0; j<this->wyckoff_entry->num; j++){
        if( (i == j) && (!if_repeat_i) )  continue;

        symbol_j = this->wyckoff_entry->sites[j].symbol;
        symmetry_j = this->wyckoff_entry->sites[j].symmetry;     
        
        sorder1 = symbol_i + " " + symbol_j;
        
        struct_symmetric_wyckoff_pairs *helper_symmetric_wyckoff_pairs = 
                                            new struct_symmetric_wyckoff_pairs;
        helper_symmetric_wyckoff_pairs->order = sorder1;
        helper_symmetric_wyckoff_pairs->symmetric_orders = 
                                                new struct_symmetric_orders;
        
        helper_symmetric_wyckoff_pairs->next = 
                                     this->symmetric_wyckoff_pairs[sg-1]->next;
        this->symmetric_wyckoff_pairs[sg-1]->next = 
                                            helper_symmetric_wyckoff_pairs;  


        // now find all pairs which could be symmetric to given pair
        // loop over all ii
        for(int ii=0; ii<this->wyckoff_entry->num; ii++){
          symbol_ii = this->wyckoff_entry->sites[ii].symbol;
          symmetry_ii = this->wyckoff_entry->sites[ii].symmetry;     
          
          if(symmetry_i != symmetry_ii)
            continue; 
      
          if_repeat_ii = false;     
          for(int c=0; c<3; c++){
            for(int ic=0; ic<3; ic++){
              if(abs(this->wyckoff_entry->sites[ii].coordinates[
                                                    12*0 + 4*c + ic]) > 
                                                            this->tolerance){
                if_repeat_ii = true;
                break;
              }
            }
            if(if_repeat_ii) break;
          }

          // loop over all jj
          for(int jj=0; jj<this->wyckoff_entry->num; jj++){
            if( (ii == jj) && (!if_repeat_ii) )  continue;

            symbol_jj = this->wyckoff_entry->sites[jj].symbol;
            symmetry_jj = this->wyckoff_entry->sites[jj].symmetry;     
          
            if(symmetry_j != symmetry_jj)
              continue;
     
            sorder2 = symbol_ii + " " + symbol_jj;

            if_same = if_wyckoff_order_related_through_symmetry(2,
                                                            sorder1, sorder2,
                                                            false);
      
            if(if_same){
              
              struct_symmetric_orders *helper_symmetric_orders = 
                                                new struct_symmetric_orders;
              helper_symmetric_orders->order = sorder2;

              helper_symmetric_orders->next = 
                        helper_symmetric_wyckoff_pairs->symmetric_orders->next;
              helper_symmetric_wyckoff_pairs->symmetric_orders->next = 
                                                      helper_symmetric_orders; 
               

            } // add to the list
          

          } // over jj

        } // over ii

      
      } //over j 
 
        
    } // over i
 
  }

#ifdef __SYSTEM_BULK_PARALLEL__
  int PE;
  MPI_Comm_rank(MPI_COMM_WORLD, &PE);
  if(PE==0)
#endif
  
  dump_symmetric_wyckoff_pairs();

}


// ---------------------------------------------------------------------------//
// ... read symmetric wyckoff pairs from a file
bool SYSTEM_BULK::read_symmetric_wyckoff_pairs(){
  string filename = this->symmetric_wyckoff_pairs_filename;
  ifstream file(filename.c_str());
  if(!file){
    return false;
  }

  int num_i, num_j;
  string line;
  stringstream sline; 
  
  // loop over all spacegroups
  for(int sg=1; sg<=this->num_spaceGroups; sg++){
    this->symmetric_wyckoff_pairs[sg-1] = new struct_symmetric_wyckoff_pairs;
    getline(file, line, '\n');
    sline.clear(); sline.str(line); 
    sline>>num_i;
    
    for(int i=0; i<num_i; i++){
      struct_symmetric_wyckoff_pairs *helper_symmetric_wyckoff_pairs = 
                                        new struct_symmetric_wyckoff_pairs;
      helper_symmetric_wyckoff_pairs->symmetric_orders = 
                                               new struct_symmetric_orders;
      helper_symmetric_wyckoff_pairs->next = 
                                 this->symmetric_wyckoff_pairs[sg-1]->next;
      this->symmetric_wyckoff_pairs[sg-1]->next = 
                                            helper_symmetric_wyckoff_pairs;

      getline(file, line, '\n');
      sline.clear(); sline.str(line);
      sline>>helper_symmetric_wyckoff_pairs->order;
      helper_symmetric_wyckoff_pairs->order[1] = ' ';
      sline>>num_j;
      
      for(int j=0; j<num_j; j++){
        struct_symmetric_orders *helper_symmetric_orders = 
                                                   new struct_symmetric_orders;
        helper_symmetric_orders->next = 
                        helper_symmetric_wyckoff_pairs->symmetric_orders->next;
        helper_symmetric_wyckoff_pairs->symmetric_orders->next = 
                                                       helper_symmetric_orders;
        sline>>helper_symmetric_orders->order; 
        helper_symmetric_orders->order[1] = ' ';
      }// j


    }//i
    
    getline(file, line, '\n');
  }//sg  

  
  return true;
}


// ---------------------------------------------------------------------------//
// ... save symmetric wyckoff pairs to a file
void SYSTEM_BULK::dump_symmetric_wyckoff_pairs(){
  string filename = this->symmetric_wyckoff_pairs_filename;
  ofstream file(filename.c_str());
  
  string order;
  int num;
  struct_symmetric_wyckoff_pairs *helper_symmetric_wyckoff_pairs;
  struct_symmetric_orders *helper_symmetric_orders;
  
  // loop over all spacegroups
  for(int sg=1; sg<=num_spaceGroups; sg++){
    
    helper_symmetric_wyckoff_pairs = this->symmetric_wyckoff_pairs[sg-1]; 
    num = 0;
    while(helper_symmetric_wyckoff_pairs->next != NULL){
      helper_symmetric_wyckoff_pairs = helper_symmetric_wyckoff_pairs->next;
      num++;
    };
    file<<num<<'\n'; 

    helper_symmetric_wyckoff_pairs = this->symmetric_wyckoff_pairs[sg-1]; 
    while(helper_symmetric_wyckoff_pairs->next != NULL){
      helper_symmetric_wyckoff_pairs = helper_symmetric_wyckoff_pairs->next;
      order = helper_symmetric_wyckoff_pairs->order;
      order[1] = '_';
      file<<order<<'\t';
      
      
      helper_symmetric_orders = 
                              helper_symmetric_wyckoff_pairs->symmetric_orders;
      num = 0;
      while(helper_symmetric_orders->next != NULL){
        helper_symmetric_orders = helper_symmetric_orders->next;
        num++;
      }
      file<<num<<'\t';
      
      helper_symmetric_orders = 
                              helper_symmetric_wyckoff_pairs->symmetric_orders;
      while(helper_symmetric_orders->next != NULL){
        helper_symmetric_orders = helper_symmetric_orders->next;
        order = helper_symmetric_orders->order;
        order[1] = '_';
        file<<order<<'\t'; 
      }
      file<<'\n';
    
    }; 
    file<<'\n';

  }// sg

  file.close();
}


// ---------------------------------------------------------------------------//
// set crystal DOFs
void SYSTEM_BULK::set_crystal_class_coordinates(){
  
  this->crystal_coordinates = new struct_crystal_coordinates[7];
  
  this->crystal_coordinates[0].symbol = 'P';
  this->crystal_coordinates[0].num = 1;
  this->crystal_coordinates[0].coordinates = new double
                                        [3*this->crystal_coordinates[0].num];
  this->crystal_coordinates[0].coordinates[3*0+0] = 0.0;
  this->crystal_coordinates[0].coordinates[3*0+1] = 0.0;
  this->crystal_coordinates[0].coordinates[3*0+2] = 0.0;

  this->crystal_coordinates[1].symbol = 'A';
  this->crystal_coordinates[1].num = 2;
  this->crystal_coordinates[1].coordinates = new double
                                        [3*this->crystal_coordinates[1].num];
  this->crystal_coordinates[1].coordinates[3*0+0] = 0.0;
  this->crystal_coordinates[1].coordinates[3*0+1] = 0.0;
  this->crystal_coordinates[1].coordinates[3*0+2] = 0.0;
  this->crystal_coordinates[1].coordinates[3*1+0] = 0.0;
  this->crystal_coordinates[1].coordinates[3*1+1] = 0.5;
  this->crystal_coordinates[1].coordinates[3*1+2] = 0.5;

  this->crystal_coordinates[2].symbol = 'C';
  this->crystal_coordinates[2].num = 2;
  this->crystal_coordinates[2].coordinates = new double
                                        [3*this->crystal_coordinates[2].num];
  this->crystal_coordinates[2].coordinates[3*0+0] = 0.0;
  this->crystal_coordinates[2].coordinates[3*0+1] = 0.0;
  this->crystal_coordinates[2].coordinates[3*0+2] = 0.0;
  this->crystal_coordinates[2].coordinates[3*1+0] = 0.5;
  this->crystal_coordinates[2].coordinates[3*1+1] = 0.5;
  this->crystal_coordinates[2].coordinates[3*1+2] = 0.0;

  this->crystal_coordinates[3].symbol = 'I';
  this->crystal_coordinates[3].num = 2;
  this->crystal_coordinates[3].coordinates = new double
                                        [3*this->crystal_coordinates[3].num];
  this->crystal_coordinates[3].coordinates[3*0+0] = 0.0;
  this->crystal_coordinates[3].coordinates[3*0+1] = 0.0;
  this->crystal_coordinates[3].coordinates[3*0+2] = 0.0;
  this->crystal_coordinates[3].coordinates[3*1+0] = 0.5;
  this->crystal_coordinates[3].coordinates[3*1+1] = 0.5;
  this->crystal_coordinates[3].coordinates[3*1+2] = 0.5;

  this->crystal_coordinates[4].symbol = 'R';
  this->crystal_coordinates[4].num = 1;
  this->crystal_coordinates[4].coordinates = new double
                                        [3*this->crystal_coordinates[4].num];
  this->crystal_coordinates[4].coordinates[3*0+0] = 0.0;
  this->crystal_coordinates[4].coordinates[3*0+1] = 0.0;
  this->crystal_coordinates[4].coordinates[3*0+2] = 0.0;


  this->crystal_coordinates[5].symbol = 'F';
  this->crystal_coordinates[5].num = 4;
  this->crystal_coordinates[5].coordinates = new double
                                        [3*this->crystal_coordinates[5].num];
  this->crystal_coordinates[5].coordinates[3*0+0] = 0.0;
  this->crystal_coordinates[5].coordinates[3*0+1] = 0.0;
  this->crystal_coordinates[5].coordinates[3*0+2] = 0.0;
  this->crystal_coordinates[5].coordinates[3*1+0] = 0.0;
  this->crystal_coordinates[5].coordinates[3*1+1] = 0.5;
  this->crystal_coordinates[5].coordinates[3*1+2] = 0.5;
  this->crystal_coordinates[5].coordinates[3*2+0] = 0.5;
  this->crystal_coordinates[5].coordinates[3*2+1] = 0.0;
  this->crystal_coordinates[5].coordinates[3*2+2] = 0.5;
  this->crystal_coordinates[5].coordinates[3*3+0] = 0.5;
  this->crystal_coordinates[5].coordinates[3*3+1] = 0.5;
  this->crystal_coordinates[5].coordinates[3*3+2] = 0.0;

  this->crystal_coordinates[6].symbol = 'H';
  this->crystal_coordinates[6].num = 3;
  this->crystal_coordinates[6].coordinates = new double
                                        [3*this->crystal_coordinates[6].num];
  this->crystal_coordinates[6].coordinates[3*0+0] = 0.0;
  this->crystal_coordinates[6].coordinates[3*0+1] = 0.0;
  this->crystal_coordinates[6].coordinates[3*0+2] = 0.0;
  this->crystal_coordinates[6].coordinates[3*1+0] = 2.0/3.0;
  this->crystal_coordinates[6].coordinates[3*1+1] = 1.0/3.0;
  this->crystal_coordinates[6].coordinates[3*1+2] = 1.0/3.0;
  this->crystal_coordinates[6].coordinates[3*2+0] = 1.0/3.0;
  this->crystal_coordinates[6].coordinates[3*2+1] = 2.0/3.0;
  this->crystal_coordinates[6].coordinates[3*2+2] = 2.0/3.0;

}



// ---------------------------------------------------------------------------//
// read Wyckoff data
void SYSTEM_BULK::read_Wyckoff_data(){
  
  string str;
  stringstream iss;
 
  // check if file exist 
  ifstream input(this->VESTA_Wyckoff_filename.c_str());
  if(!input.is_open()){
    cout<<"Cannot open "<<this->VESTA_Wyckoff_filename<<" file to read \
                          Wyckoff! "<<'\n';
    exit(0); 
  }

  // read number of enteries
  getline(input, str, '\n'); 
  iss.clear(); iss.str(str);
  iss>>this->wyckoff_dict.num; 

  // loop over enteries
  this->wyckoff_dict.entries = new struct_wyckoff_entry[this->wyckoff_dict.num]; 
  for(int i=0; i<this->wyckoff_dict.num;i++){
    getline(input, str, '\n');    
    iss.clear(); iss.str(str);
    iss >> this->wyckoff_dict.entries[i].origin_choice
        >> this->wyckoff_dict.entries[i].spaceGroupNumber
        >> this->wyckoff_dict.entries[i].spaceGroupName
        >> this->wyckoff_dict.entries[i].num;
    
    this->wyckoff_dict.entries[i].sites = new struct_wyckoff_site[
            this->wyckoff_dict.entries[i].num];
   
    // loop over sites 
    for(int j=0;j<this->wyckoff_dict.entries[i].num;j++){
      getline(input, str, '\n');    
      iss.clear(); iss.str(str);
      iss >> this->wyckoff_dict.entries[i].sites[j].multiplicity
          >> this->wyckoff_dict.entries[i].sites[j].symbol
          >> this->wyckoff_dict.entries[i].sites[j].symmetry
          >> this->wyckoff_dict.entries[i].sites[j].num;
      
      this->wyckoff_dict.entries[i].sites[j].coordinates = 
              new double [this->wyckoff_dict.entries[i].sites[j].num * 12];
      
      // loop over all enteires in given site
      for(int k=0; k<this->wyckoff_dict.entries[i].sites[j].num; k++){
        getline(input, str, '\n');    
        iss.clear(); iss.str(str);

        // loop over coordinates
        for(int l=0; l<12; l++){
          iss >> this->wyckoff_dict.entries[i].sites[j].coordinates[12*k + l]; 
        }
      }
       
    }
    
    getline(input, str, '\n');    // blank line
  }


  input.close();
  
}


// ---------------------------------------------------------------------------//
// read vdw data
void SYSTEM_BULK::read_vdw_data(){
  
  string str;
  stringstream iss;
 
  // check if file exist 
  ifstream input(this->vdw_filename.c_str());
  int count = 0;
  while(getline(input, str, '\n')) count++;

  this->vdw_dict.num = count;
  this->vdw_dict.entries = new struct_vdw_entry[this->vdw_dict.num];
  input.clear();
  input.seekg(0);

  // read all vdw data
  for(int i=0; i<this->vdw_dict.num; i++){
    getline(input, str, '\n');
    iss.clear();  iss.str(str);
    iss >> this->vdw_dict.entries[i].atomic_number
        >> this->vdw_dict.entries[i].symbol
        >> this->vdw_dict.entries[i].C
        >> this->vdw_dict.entries[i].R;
  }

  input.close();

}


// ---------------------------------------------------------------------------//
// ..... set spaceGroup
void SYSTEM_BULK::set_spacegroup(int spaceGroupNumber, bool reset){

  if(reset) this->reset_spacegroup();
  
  // check if SG is valid
  if(spaceGroupNumber < 0 || spaceGroupNumber > 230){
    cout<<"Invalid SPaceGroup Number! "<<spaceGroupNumber<<'\n';
    this->spaceGroupNumber = 0;
    return;
  }
  this->spaceGroupNumber = spaceGroupNumber;
  
  // set the wyckoff entry now   
  for(int i=0; i<this->wyckoff_dict.num; i++){
    if(this->spaceGroupNumber == 
                          this->wyckoff_dict.entries[i].spaceGroupNumber){
      this->wyckoff_entry = &this->wyckoff_dict.entries[i];
      break;
    }
  }

  // set the crystal_coordinate_entry now
  for(int i=0; i<7; i++){
    if(this->crystal_coordinates[i].symbol ==
                            this->wyckoff_entry->spaceGroupName[0]){
      this->crystal_coordinate_entry = &this->crystal_coordinates[i];
    }
  }
  // reset the crystal_coordinate_entry for R
  if(this->crystal_coordinate_entry->symbol == 'R'){  
    for(int i=0; i<7; i++){
      if(this->crystal_coordinates[i].symbol == 'H'){
        this->crystal_coordinate_entry = &this->crystal_coordinates[i];
      }
    }
  }

}

// ---------------------------------------------------------------------------//
void SYSTEM_BULK::get_species_permutations(int *num, string **permutations){
  if(this->species_permutations == NULL){
    cout<<"Species-permutations not set!"<<'\n';
    *num = 0;
    return;
  }
  *num = this->num_species_permutations;
  *permutations = new string [*num];
  for(int i=0; i<*num; i++){
    (*permutations)[i] = this->species_permutations[i];
  }
}


// ---------------------------------------------------------------------------//
void SYSTEM_BULK::get_name(string *name){
  if(this->name == ""){
    cout<<"Species not set!"<<'\n';
    *name = "";
    return;
  }
  *name = this->name;
}


// ---------------------------------------------------------------------------//
void SYSTEM_BULK::get_parameter_values(int *num, string **name, double **value){
  if(this->parameters == NULL){
    cout<<"Wyckoff not set!"<<'\n';
    *num = 0;
    return;
  }
  if(this->parameter_values == NULL){
    cout<<"parameter_values not set!"<<'\n';
    *num = 0;
    return;
  }

  *num = this->num_parameters;
  *name = new string [*num];
  *value = new double [*num];
  for(int i=0; i<*num; i++){
    (*name)[i] = this->parameters[i];
    (*value)[i] = this->parameter_values[i];
  }
}


// ---------------------------------------------------------------------------//
void SYSTEM_BULK::get_parameters(int *num, string **parameters){
  if(this->parameters == NULL){
    cout<<"Wyckoff not set!"<<'\n';
    *num = 0;
    return;
  }
  *num = this->num_parameters;
  *parameters = new string [*num];
  for(int i=0; i<*num; i++){
    (*parameters)[i] = this->parameters[i];
  }
}


// ---------------------------------------------------------------------------//
void SYSTEM_BULK::reset_spacegroup(){
  this->wyckoff_entry = NULL;
  this->spaceGroupNumber = 0;
  this->reset_wyckoff();
}

// ---------------------------------------------------------------------------//
void SYSTEM_BULK::reset_wyckoff(){
  if(this->wyckoff != NULL){
    delete [] this->wyckoff;
    this->wyckoff = NULL;
  }
  if(this->wyckoff_orders != NULL){
    delete [] this->wyckoff_orders;
    this->wyckoff_orders = NULL;
  }
  if(this->parameters != NULL){
    delete [] this->parameters;
    this->parameters = NULL;
  }
  if(this->basis_parameters != NULL){
    delete [] this->basis_parameters;
    this->basis_parameters = NULL;
  }
  if(this->lattice_parameters != NULL){
    delete [] this->lattice_parameters;
    this->lattice_parameters = NULL;
  }
  this->reset_species();
}

// ---------------------------------------------------------------------------//
void SYSTEM_BULK::reset_species(){
  if(this->species != NULL){ 
    delete [] this->species;
    this->species = NULL;
  }
  this->name = "";
  if(this->species_permutations != NULL){
    delete [] this->species_permutations;
    this->species_permutations = NULL;
  }
  this->reset_parameter_values();
}

// ---------------------------------------------------------------------------//
void SYSTEM_BULK::reset_parameter_values(){
  if(this->lattice != NULL){
    delete this->lattice;
    this->lattice = NULL; 
  }
  if(this->primitive_lattice != NULL){
    delete this->primitive_lattice;
    this->primitive_lattice = NULL;
  }
  if(this->std_lattice != NULL){
    delete this->std_lattice;
    this->std_lattice = NULL;
  }
}


// ---------------------------------------------------------------------------//
// Constructor
SYSTEM_BULK::SYSTEM_BULK(double tolerance, string path){
  this->tolerance = tolerance;
  this->data_directory = path + "/./DATA/";
  this->VESTA_Wyckoff_filename = this->data_directory + "./Wyckoff.dat";
  this->vdw_filename = this->data_directory + "./vDW.dat";
  this->symmetric_wyckoff_pairs_filename = 
                    this->data_directory + "./Symmetric_Wyckoff_Pairs.dat";
  this->vdw_cutoff = 15.; 
 
  this->max_wyckoffs = 6;
  
  // initialize other parameters
  this->spaceGroupNumber = 0;
  this->wyckoff_entry = NULL;
  this->num_lattice_parameters = 0;
  this->num_basis_parameters = 0;
  this->num_parameters = 0;
  this->lattice_parameters = NULL;
  this->basis_parameters = NULL;
  this->parameters = NULL;
  this->trigonal_flag = "hexagonal";
  this->wyckoff = NULL;
  this->num_wyckoff = 0;
  this->species = NULL;
  this->num_species = 0;
  this->num_wyckoff_orders = 0;
  this->wyckoff_orders = NULL;
  this->name = "";
  this->num_species_permutations = 0;
  this->species_permutations = NULL;
  this->lattice = NULL;
  this->primitive_lattice = NULL;
  this->std_lattice = NULL;
  this->symmetry_dataset = NULL;
  this->lattice_parameter_values = NULL;
  this->basis_parameter_values = NULL;
  this->parameter_values = NULL;
  this->symmetric_wyckoff_pairs = NULL; 

  // initialize pointers
  this->crystal_coordinates = NULL;
  
  // read Wyckoff and vDW data
  read_Wyckoff_data();
  set_crystal_class_coordinates();
  read_vdw_data(); 
  this->num_spaceGroups = 230;
  generate_symmetric_wyckoff_pairs();
    
}


// ---------------------------------------------------------------------------//
// Destructor
SYSTEM_BULK::~SYSTEM_BULK(){

  // free memory if allocated
  if(this->crystal_coordinates != NULL){
    delete [] this->crystal_coordinates;
    this->crystal_coordinates = NULL;
  }
  if(this->lattice_parameters != NULL){
    delete [] this->lattice_parameters;
    this->lattice_parameters = NULL;
  }
  if(this->basis_parameters != NULL){
    delete [] this->basis_parameters;
    this->basis_parameters = NULL;
  }
  if(this->parameters != NULL){
    delete [] this->parameters;
    this->parameters = NULL;
  }
  if(this->wyckoff != NULL){
    delete [] this->wyckoff;
    this->wyckoff = NULL;
  }
  if(this->species != NULL){
    delete [] this->species;
    this->species = NULL;
  }
  if(this->wyckoff_orders != NULL){
    delete [] this->wyckoff_orders;
    this->wyckoff_orders = NULL;
  }
  if(this->species_permutations != NULL){
    delete [] this->species_permutations;
    this->species_permutations = NULL;
  }
  if(this->lattice != NULL){
    delete this->lattice;
    this->lattice = NULL;
  }
  if(this->primitive_lattice != NULL){
    delete this->primitive_lattice;
    this->primitive_lattice = NULL;
  }
  if(this->std_lattice != NULL){
    delete this->std_lattice;
    this->std_lattice = NULL;
  }
  if(this->symmetry_dataset != NULL){
    spg_free_dataset(this->symmetry_dataset);
    this->symmetry_dataset = NULL;
  }
  if(this->lattice_parameter_values != NULL){
    delete [] this->lattice_parameter_values;
    this->lattice_parameter_values = NULL;
  }
  if(this->basis_parameter_values != NULL){
    delete [] this->basis_parameter_values;
    this->basis_parameter_values = NULL;
  }
  if(this->parameter_values != NULL){
    delete [] this->parameter_values;
    this->parameter_values = NULL;
  }
  if(this->symmetric_wyckoff_pairs != NULL){
    for(int i=0;i<this->num_spaceGroups; i++){
      struct_symmetric_wyckoff_pairs *helper_symmetric_wyckoff_pairs;
      helper_symmetric_wyckoff_pairs = symmetric_wyckoff_pairs[i];
      while(symmetric_wyckoff_pairs[i]->next != NULL){
        symmetric_wyckoff_pairs[i] = symmetric_wyckoff_pairs[i]->next;
        delete helper_symmetric_wyckoff_pairs;
        helper_symmetric_wyckoff_pairs = symmetric_wyckoff_pairs[i];
      }
      delete symmetric_wyckoff_pairs[i]; 
    }
    delete [] symmetric_wyckoff_pairs;
  }

  return;
}

