#include "common.h"
#include "system_bulk.h"

extern "C" {
  
  SYSTEM_BULK *bulk_new(double tolerance, char *path){
    SYSTEM_BULK *bulk = new SYSTEM_BULK(tolerance, string(path));
    return bulk; 
  };

  void bulk_delete(SYSTEM_BULK *bulk){
    delete bulk;
  }

  void bulk_set_structure_from_file(SYSTEM_BULK *bulk, char *file_content){
    
    bulk->set_structure_from_file(string(file_content));
  }

  int bulk_get_spacegroup(SYSTEM_BULK *bulk){
    return bulk->get_spacegroup();
  }
  
  void bulk_get_wyckoff(SYSTEM_BULK *bulk, int *num, char ***wyckoff){
    
    string *temp_wyckoff = NULL;
    bulk->get_wyckoff(num, &temp_wyckoff);
    
    *wyckoff = new char* [*num];
    for(int i=0; i<*num; i++){
      (*wyckoff)[i] = new char [temp_wyckoff[i].length() + 1];
      strcpy((*wyckoff)[i], temp_wyckoff[i].c_str());
      (*wyckoff)[i][temp_wyckoff[i].length()] = '\0';
    }

    if(temp_wyckoff != NULL)
      delete [] temp_wyckoff;

  }


  void bulk_free_wyckoff(int num, char **wyckoff){
    if(num == 0) return;
    for(int i=0; i<num; i++)
      delete [] wyckoff[i];
    delete [] wyckoff;  
  }


  void bulk_get_species(SYSTEM_BULK *bulk, int *num, char ***species){
    
    string *temp_species = NULL;
    bulk->get_species(num, &temp_species);
    
    *species = new char* [*num];
    for(int i=0; i<*num; i++){
      (*species)[i] = new char [temp_species[i].length() + 1];
      strcpy((*species)[i], temp_species[i].c_str());
      (*species)[i][temp_species[i].length()] = '\0';
    }

    if(temp_species != NULL)
      delete [] temp_species;

  }

  void bulk_free_species(int num, char **species){
    if(num == 0) return;
    for(int i=0; i<num; i++)
      delete [] species[i];
    delete [] species;
  }

  void bulk_get_name(SYSTEM_BULK *bulk, char **name){
    
    string temp_name = "";
    bulk->get_name(&temp_name);

    (*name) = new char [temp_name.length() + 1];
    strcpy((*name), temp_name.c_str());
    (*name)[temp_name.length()] = '\0';
   
  }
  
  void bulk_free_name(char *name){
    delete [] name;
  }

}
