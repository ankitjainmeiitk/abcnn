#include "common.h" 


// ---------------------------------------------------------------------------//
//  ........ find distance between two atoms
double find_distance_square_between_atoms(ATOM a1, ATOM a2){
  double dist = 0.0;
  for(int d=0; d<3; d++)
    dist += (a1.cart[d] - a2.cart[d]) * (a1.cart[d] - a2.cart[d]);
  return dist;  
}


// ---------------------------------------------------------------------------//
// convert into to string
string int_to_str(int i){
  stringstream iss;
  iss<<i;
  return iss.str();
}

// ---------------------------------------------------------------------------//
// convert into to string
string float_to_str(float f){
  stringstream iss;
  iss<<f;
  return iss.str();
}


// ---------------------------------------------------------------------------//
// split string into array of num strings based on delimiter (space, tab)
void split_string_into_string_array(string str, int num, 
          string **array){
  
  *array = new string [num];
  stringstream sline;
  sline.clear(); sline.str(str);
  for(int i=0; i<num; i++) sline>>(*array)[i];

}



// ---------------------------------------------------------------------------//
// get gcd of numbers
int calculate_gcd(int a, int b){
  if(a == 0 || b == 0) return 0;
  if(a == b) return a;
  if(a > b) return calculate_gcd(a-b, b);
  return calculate_gcd(a, b-a);
}


// ---------------------------------------------------------------------------//
// get cross product
void calculate_cross_product(double *a, double *b, double *c){
  for(int d=0;d<3;d++) c[d] = 0.0;
  c[0] = a[1]*b[2] - a[2]*b[1];
  c[1] = a[2]*b[0] - a[0]*b[2];
  c[2] = a[0]*b[1] - a[1]*b[0];
}


// ---------------------------------------------------------------------------//
void calculate_matrix_inverse(double *a, double *b){
    
  // get the inv_alat now
  MatrixXd II(3,3);
  MatrixXd inv_II(3,3);
  for(int i=0;i<3;i++){
    for(int j=0;j<3;j++){
      II(i,j) = a[3*i+j];
    }
  }
  inv_II = II.inverse();
  for(int i=0;i<3;i++){
    for(int j=0;j<3;j++){
      b[3*i+j] = inv_II(i,j);
    }
  }
}


// ---------------------------------------------------------------------------//
void calculate_matmul(double *A, double *B, double *C){
  for(int d=0;d<9;d++) C[d] = 0.0;
  for(int d1=0; d1<3; d1++){
    for(int d2=0; d2<3; d2++){
      for(int d3=0; d3<3; d3++){
        C[3*d1+d3] += A[3*d1+d2]*B[3*d2+d3]; 
      }
    }
  }
}
    
    
    
// ---------------------------------------------------------------------------//
// ...... choose k of n integers
void n_choose_k_int(int n, int k, int *list, int *nentry, int **entry){
  
  /*
    n - size of list
    k - k-elements to be choosen
    nentry -number of ways they can be chosen
    entry - ways
  */

  if(k == 1){
    *nentry = n;
    *entry = new int [*nentry];
    for(int i=0; i<*nentry; i++) (*entry)[i] = list[i];
    return;
  }

  int temp_nentry;
  int *temp_entry;
  int *temp_list = new int [n-1];

  *nentry = 0;
  *entry = NULL;
  int *mem_entry;

  for(int i=0; i<n; i++){
    for(int j=0; j<i; j++) temp_list[j] = list[j];
    for(int j=i; j<n-1; j++) temp_list[j] = list[j+1];
    n_choose_k_int(n-1, k-1, temp_list, &temp_nentry, &temp_entry);
  
    mem_entry = *entry;
    *entry = new int [(*nentry + temp_nentry) * k];
    for(int ii=0; ii<(*nentry); ii++){
      for(int jj=0; jj<k; jj++) (*entry)[k*ii + jj] = mem_entry[k*ii+jj];
    }

    for(int ii=0; ii<temp_nentry; ii++){
      (*entry)[k*(*nentry + ii) + 0] = list[i];
      for(int jj=0; jj<k-1; jj++){
        (*entry)[k*(*nentry+ii) + jj+1] = temp_entry[(k-1)*ii + jj];
      }
    }
    *nentry += temp_nentry;

    delete [] mem_entry;
    delete [] temp_entry;
  }


  delete [] temp_list;
  
  return;
}



// ---------------------------------------------------------------------------//
// get progress string
string get_progress_string(double total_progress, double current_progress){

  string str = "";

  int barWidth = 60;
  int pos = (int)(barWidth * current_progress / total_progress);
  str += "\r";
  str += "[";
  for(int i=0; i<barWidth; i++){
    if(i<pos) str += "=";
    else if(i == pos) str += ">";
    else str += " "; 
  }
  str += "] ";
  str += int_to_str(round(current_progress));
  str += " of ";
  str += int_to_str(round(total_progress));
  
  return str;  
}


// ---------------------------------------------------------------------------//
// get random number in given range
int get_random_int_in_range(int given_min, int given_max){

  if(given_max < given_min){
    cout<<"Oops. Min should be less than max!"<<'\n';
    return 0; 
  }

  int num = given_min + 
                round( (((double)rand())/RAND_MAX) * (given_max - given_min) );
  
  return num;  
}


// ---------------------------------------------------------------------------//
// get random number in given range
double get_random_double_in_range(double given_min, double given_max){

  if(given_max < given_min){
    cout<<"Oops. Min should be less than max!"<<'\n';
    return 0.0; 
  }

  return given_min + (((double)rand())/RAND_MAX) * (given_max - given_min);
}


// ---------------------------------------------------------------------------//
// read structure ... accept only VASP format
void read_structure(string file_content, LATTICE *lattice, double tolerance){
 
  stringstream inp(file_content);
 
  string line;
  stringstream sline;
  size_t find;
 
  string comment;
  
  // read scaling_factor and alat
  double scaling_factor;
  getline(inp, comment, '\n');
  getline(inp,line,'\n');
  sline.str(line);
  sline>>scaling_factor;
  for(int i=0; i<3; i++){
    getline(inp, line, '\n');
    sline.clear();
    sline.str(line);
    sline>>lattice->alat[3*i+0]>>lattice->alat[3*i+1]>>lattice->alat[3*i+2];
  }   
  // scale lattice vectors by scaling factor
  for(int i=0; i<9; i++) lattice->alat[i] *= scaling_factor;

  // check if next line contains symbol string
  bool if_symbols;
  string symbols;
  getline(inp, line, '\n');
  find =
     line.find_first_of("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
  if(find != std::string::npos){
    if_symbols = true;
    symbols = line;
    getline(inp, line, '\n');
    sline.clear();
    sline.str(line);
  }else{
    if_symbols = false;
    sline.clear();
    sline.str(line);
  }  
  string atoms;
  getline(sline, atoms, '\n');

  // count number of atoms now
  int itemp;
  lattice->natom = 0;
  line = atoms;
  while((find = line.find_first_of("123456789")) != string::npos){
    line = line.substr(find);
    sline.clear();
    sline.str(line);
    sline>>itemp;
    lattice->natom += itemp;
    find = line.find_first_of(" \t");
    if(find != string::npos) line = line.substr(find+1);
    else break; 
  };
 
  
  // remove selective dynamics line
  bool if_selective_dynamics = false;
  getline(inp, line, '\n');
  find = line.find_first_not_of(" ");
  if(line[find] == 'S' || line[find] == 's'){
    getline(inp, line, '\n');   
    if_selective_dynamics = true;
  }

  // see if positions are in cart or in direct
  bool if_cart;
  find = line.find_first_not_of(" ");
  if(line[find] == 'D' || line[find] == 'd') if_cart = false;
  else if(line[find] == 'C' || line[find] == 'c') if_cart = true;
  else{
    cout<<"I cannot decide if positions are in cartesian" 
                      "or in direct coordinates."<<'\n';
    return;
  }
  
  // read positions now
  lattice->basis = new ATOM [lattice->natom];
  for(int i=0; i<lattice->natom; i++){
    getline(inp, line, '\n');
    sline.clear();
    sline.str(line);
    for(int d=0; d<3; d++){
      if(if_cart) sline >> lattice->basis[i].cart[d];
      else sline >> lattice->basis[i].direct[d];
    }
    if(if_selective_dynamics){
      string tt;
      tt = sline.str();
      int loc;
      for(int d=0; d<3; d++){
        loc = tt.find_first_of("fFTt");
        if(tt[loc] == 'f' || tt[loc] == 'F') 
          lattice->basis[i].fix[d] = 1;
        else
          lattice->basis[i].fix[d] = 0;
        tt = tt.substr(loc + 1);
      }
      
    }else{
      for(int d=0; d<3; d++) lattice->basis[i].fix[d] = 0;
    }
      
  }
 
  // fill the other [cart/dircet] positions now
  set_inv_alat(lattice);
  if(if_cart == false) cart_to_direct(lattice, -1);
  else  cart_to_direct(lattice, 1);

  // figure out symbols now
  string alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

  // count ntype
  line = atoms;
  int ntype = 0;
  while((find = line.find_first_of("123456789")) != string::npos){
    line = line.substr(find);
    sline.clear();
    sline.str(line);
    sline>>itemp;
    ntype += 1;
    find = line.find_first_of(" \t");
    if(find != string::npos) line = line.substr(find+1);
    else break;
  };

  // get types now
  int *type = new int [ntype];
  string *symbol = new string [ntype];
  ntype = 0;
  while((find = atoms.find_first_of("123456789")) != string::npos){
    atoms = atoms.substr(find);
    sline.clear();
    sline.str(atoms);
    sline>>type[ntype];
    ntype += 1;
    find = atoms.find_first_of(" \t");
    if(find != string::npos) atoms = atoms.substr(find+1);
    else break;
  };

  // see if symbols can be read from first line
  if(if_symbols == false){
    bool if_symbols_in_comment = false;

    string temp_comment = comment;
    int symbol_comment_count = 0;
    while((find = temp_comment.find_first_of("abcdefghijklmnopqrst"
                                        "uvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")) != 
                                        string::npos){

      temp_comment = temp_comment.substr(find+1);
      find = temp_comment.find_first_not_of("abcdefghijklmnopqrstuvwxyz"
                                            "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
      temp_comment = temp_comment.substr(find+1);
      symbol_comment_count++;

    };


    if(symbol_comment_count == ntype)
      if_symbols_in_comment = true;

    if(if_symbols_in_comment){
      if_symbols = true;
      symbols = comment;
    }
  }

  // get symbols now
  if(if_symbols == true){
    sline.clear();
    sline.str(symbols);
    for(int i=0; i<ntype; i++){
      sline>>symbol[i];    
    }
  }else{
    for(int i=0; i<ntype; i++){
      symbol[i] = alphabets[i%26] + int_to_str(i); 
    }
  }
  
  // now put the symbols in ATOM
  ntype = 0;
  int count = 0;
  for(int i=0; i<lattice->natom; i++){
    lattice->basis[i].symbol = symbol[ntype];
    count++;
    if(count == type[ntype]){
      count = 0;
      ntype++; 
    }
  }
  
    
  delete [] type; type = NULL;
  delete [] symbol; symbol = NULL;

 
  
  // check if atomic positions are valid, i.e., min distance between atoms 
  //  is greater than 5 times tolerance
  bool if_valid = true;
  double dist, pos[3], cart_pos[3];
  int N[3];

  for(int i=0; i<lattice->natom; i++){
    if(!if_valid) break;
    for(int j=i+1; j<lattice->natom; j++){ 
      if(!if_valid) break;
      
      for(int n1=-1; n1<=1; n1++){
        if(!if_valid) break;
        
        for(int n2=-1; n2<=1; n2++){
          if(!if_valid) break;
          
          for(int n3=-1; n3<=1; n3++){
            if(!if_valid) break;
            
            N[0] = n1; N[1] = n2; N[2] = n3;
        
       
            for(int d=0; d<3; d++){
              pos[d] = N[d] + 
                          (lattice->basis[i].direct[d] - 
                                lattice->basis[j].direct[d]);
            }
            
            for(int d=0; d<3; d++){
              cart_pos[d] = 0.0;
              for(int d1=0; d1<3; d1++){
                cart_pos[d] += pos[d1] * lattice->alat[3*d1+d];
              }
            }
            
            dist = 0.0;
            for(int d=0; d<3; d++){
              dist += cart_pos[d] * cart_pos[d];
            }

          
            if(dist < (5*tolerance) * (5*tolerance)){
              if_valid = false;
              break;
            }

          } //n3
        } //n2
      }// n1

    } //j
  } // i

  
  // if not a valid structure
  if(!if_valid){
    delete [] lattice->basis;
    lattice->natom = 0; 
    cout<<"========================================================="<<'\n';
    cout<<"Seems like structure has overlapping atoms! Please Check!"<<'\n';
    cout<<"========================================================="<<'\n';
  }

}



// ---------------------------------------------------------------------------//
// from cart_to_diret and vic versa
void cart_to_direct(LATTICE *lattice, int flag){
  // falg == 1 cart to direct
  // flag == -1 direct to cart


  if(flag == 1){
  
    for(int i=0; i<lattice->natom; i++){
      for(int d=0; d<3; d++) lattice->basis[i].direct[d] = 0.0;
      for(int d=0; d<3; d++){
        for(int d1=0; d1<3; d1++){
          lattice->basis[i].direct[d] += lattice->basis[i].cart[d1] *
                                                lattice->inv_alat[3*d1 + d];
        }
      }
    }
  }
  
  if(flag == -1){
    for(int i=0; i<lattice->natom; i++){
      for(int d=0; d<3; d++) lattice->basis[i].cart[d] = 0.0;
      for(int d=0; d<3; d++){
        for(int d1=0; d1<3; d1++){
          lattice->basis[i].cart[d] += lattice->basis[i].direct[d1] *
                    lattice->alat[3*d1+d];
        }
      }
    }
  }

}


// ---------------------------------------------------------------------------//
void set_inv_alat(LATTICE *lattice){
    
  // get the inv_alat now
  MatrixXd II(3,3);
  MatrixXd inv_II(3,3);
  for(int i=0;i<3;i++){
    for(int j=0;j<3;j++){
      II(i,j) = lattice->alat[3*i+j];
    }
  }
  inv_II = II.inverse();
  for(int i=0;i<3;i++){
    for(int j=0;j<3;j++){
      lattice->inv_alat[3*i+j] = inv_II(i,j);
    }
  }
}



// ---------------------------------------------------------------------------//
// get poscar string from lattice
string get_poscar_from_lattice(LATTICE *lattice, 
                                                bool selective_dynamics){
  
  if(lattice == NULL) return "";
  if(lattice->basis == NULL) return ""; 
  
  stringstream poscar;
  poscar.clear(); poscar.str("");
  
  // handle alat first
  poscar<<"Auto-generated "<<'\n';
  poscar<<"1.0"<<'\n';
  for(int i=0; i<3; i++){
    for(int d=0;d<3;d++){
      poscar<<lattice->alat[3*i+d]<<" ";
    }
    poscar<<'\n';
  }
 
  // ---------------- symbols and count next
  // need to group atoms by types
  set<string> set_symbol;
  multiset<string> multiset_symbol;
  set<string>::iterator it_symbol;   
  for(int i=0; i<lattice->natom; i++){
    set_symbol.insert(lattice->basis[i].symbol);
    multiset_symbol.insert(lattice->basis[i].symbol);
  }

  stringstream symbol_string, count_string;
  symbol_string.clear(); count_string.clear(); 
  symbol_string.str(""); count_string.str(""); 
  for(it_symbol = set_symbol.begin(); it_symbol != set_symbol.end(); 
                                                          ++it_symbol){
    symbol_string<<*it_symbol<<" ";
    count_string<<multiset_symbol.count(*it_symbol)<<" ";    
  }
  symbol_string<<'\n';
  count_string<<'\n';
  poscar << symbol_string.str();
  poscar << count_string.str();

  // now sset selective dynamics if needed
  if(selective_dynamics) poscar << "SELECTIVE_DYNAMICS"<<'\n';  
  poscar << "Direct"<<'\n';

  // now add atoms
  for(it_symbol = set_symbol.begin(); it_symbol != set_symbol.end(); 
                                                            ++it_symbol){
    for(int i=0; i<lattice->natom; i++){
      if(*it_symbol == lattice->basis[i].symbol){
        for(int d=0;d<3;d++) poscar << float_to_str(lattice->basis[i].direct[d])
                                                                 + " ";
        if(selective_dynamics){
          for(int d=0;d<3;d++){
            if(lattice->basis[i].fix[d])  poscar << "F ";
            else poscar << "T ";
          }
        }
        poscar << '\n';
      }
    }
  }


  return poscar.str();
}


// ---------------------------------------------------------------------------//
// create supercell
void create_supercell(LATTICE *lattice, LATTICE **super, int *N){
  
  for(int d=0;d<3;d++){
    if(N[d] <= 0){
      cout<<"Invalid entry for repetitions!"<<'\n';
      return;
    }
  }

  if(*super != NULL){
    delete *super;
    *super = NULL;
  }
  LATTICE *superlattice = new LATTICE;
  
  int natom = lattice->natom;
  for(int i=0; i<3; i++) natom *= N[i];

  for(int d=0;d<9;d++) superlattice->alat[d] = lattice->alat[d];
  superlattice->natom = natom;
  superlattice->basis = new ATOM [natom];
  natom = 0;
  for(int n1=-(N[0] - 1 - N[0]/2); n1<=(N[0]/2); n1++){
    for(int n2=-(N[1] - 1 - N[1]/2); n2<=(N[1]/2); n2++){
      for(int n3=-(N[2] - 1 - N[2]/2); n3<=(N[2]/2); n3++){
        for(int i=0; i<lattice->natom;i++){
          superlattice->basis[natom].symbol = lattice->basis[i].symbol;
          for(int d=0;d<3;d++){
            superlattice->basis[natom].fix[d] = lattice->basis[i].fix[d];
            superlattice->basis[natom].direct[d] = lattice->basis[i].direct[d];
          }
          superlattice->basis[natom].direct[0] += n1;
          superlattice->basis[natom].direct[1] += n2;
          superlattice->basis[natom].direct[2] += n3;
          natom++;
        }
      }
    }
  }
  
  set_inv_alat(superlattice);
  cart_to_direct(superlattice, -1);
  
  for(int i=0;i<3;i++){
    for(int d=0;d<3;d++){
      superlattice->alat[3*i+d] = N[i] * lattice->alat[3*i+d];
    }
  }
  
  set_inv_alat(superlattice);
  cart_to_direct(superlattice, 1);
  *super = superlattice;

}


// ---------------------------------------------------------------------------//
// copy lattice from 1 to 2
void copy_lattice(LATTICE *l1, LATTICE **l2){
  
  if(*l2 != NULL){
    delete *l2;
    *l2 = NULL;
  }
  
  if(l1 == NULL)
    return;

  *l2 = new LATTICE;
  (*l2)->natom = l1->natom;
  for(int d=0;d<9;d++){
    (*l2)->alat[d] = l1->alat[d];
    (*l2)->inv_alat[d] = l1->inv_alat[d];
  }

  (*l2)->basis = new ATOM [(*l2)->natom];
  for(int i=0; i<(*l2)->natom; i++){
    (*l2)->basis[i].symbol = l1->basis[i].symbol;
    for(int d=0;d<3;d++){
      (*l2)->basis[i].direct[d] = l1->basis[i].direct[d];
      (*l2)->basis[i].cart[d] = l1->basis[i].cart[d];
      (*l2)->basis[i].fix[d] = l1->basis[i].fix[d];
    }
  }

}
