#ifndef __SYSTEM_BULK_H 
#define __SYSTEM_BULK_H 

#include "system_common.h"


class SYSTEM_BULK : public SYSTEM_COMMON{
  
  private:
    string VESTA_Wyckoff_filename;  // Wyckoff filename
    string vdw_filename;            // vdw filename
    double vdw_cutoff;                  // cutoff for vdw calculations

    int max_wyckoffs;

    //-------------------------------------------------     
    // wyckoff site
    struct struct_wyckoff_site{
      string symbol;
      string symmetry;
      double *coordinates;
      int multiplicity;
      int num;
      
      struct_wyckoff_site(){
        coordinates = NULL;
        num = 0;
      }

      ~struct_wyckoff_site(){
        if(coordinates != NULL){
          delete [] coordinates;
          coordinates = NULL;
        }
      }

    };
      
    // wyckoff entry
    struct struct_wyckoff_entry{
      struct_wyckoff_site *sites;
      string spaceGroupName;
      int num;
      int origin_choice;
      int spaceGroupNumber;
    
      struct_wyckoff_entry(){
        sites = NULL;
        num = 0;
      }

      ~struct_wyckoff_entry(){
        if(sites != NULL){
          delete [] sites;
          sites = NULL;
        }
      }

    };

    // wyckoff dictionary
    struct struct_wyckoff_dict{
      struct_wyckoff_entry *entries;  
      int num;

      struct_wyckoff_dict(){
        entries = NULL;
        num = 0;
      }

      ~struct_wyckoff_dict(){
        if(entries != NULL){
          delete [] entries;
          entries = NULL;
        }
      }

    };
    struct_wyckoff_dict wyckoff_dict;
   
    
    // crystal_class_coordinates
    struct struct_crystal_coordinates{
      double *coordinates;
      int num;
      char symbol;
      
      struct_crystal_coordinates(){
        coordinates = NULL;
        num = 0;
      }

      ~struct_crystal_coordinates(){
        if(coordinates != NULL){
          delete [] coordinates;
          coordinates = NULL;
        }
      }

    };
    struct_crystal_coordinates *crystal_coordinates;

    //-------------------------------------------------     
    // vdw data
    struct struct_vdw_entry{
      string symbol;
      double C;
      double R;
      int atomic_number;
    };

    struct struct_vdw_dict{
      struct_vdw_entry *entries;
      int num; 

      struct_vdw_dict(){
        num = 0 ;
        entries = NULL;
      }

      ~struct_vdw_dict(){
        if(entries != NULL){
          delete [] entries;
          entries = NULL;
        }
      }

    };
    struct_vdw_dict vdw_dict;


    //-------------------------------------------------     
    // symmetric orders
    struct struct_symmetric_orders{
      string order;
      struct_symmetric_orders *next;

      struct_symmetric_orders(){
        order = "";
        next = NULL;
      };
      
      ~struct_symmetric_orders(){
      };

    };

    //-------------------------------------------------     
    // symmetric_wyckoff_pairs
    struct struct_symmetric_wyckoff_pairs{
      string order;
      struct_symmetric_orders *symmetric_orders;
      struct_symmetric_wyckoff_pairs *next;

      struct_symmetric_wyckoff_pairs(){
        order = ""; 
        next = NULL;
        symmetric_orders = NULL;
      }
      
      ~struct_symmetric_wyckoff_pairs(){

        if(symmetric_orders != NULL){  
          struct_symmetric_orders *helper_symmetric_orders;
          helper_symmetric_orders = symmetric_orders;
          while(symmetric_orders->next != NULL){
            symmetric_orders = symmetric_orders->next;
            delete helper_symmetric_orders;
            helper_symmetric_orders = symmetric_orders;
          }
          delete symmetric_orders; 
        }

      }

    };

    // define temp data structure
    struct struct_specie_wyckoff{
      string *wyckoff;
      string element;
      string wyckoff_string;
      int num_atom;
      int num_wyckoff;
    
      struct_specie_wyckoff(){
        num_atom = 0;
        num_wyckoff = 0;
        wyckoff = NULL;
      }
   
      ~struct_specie_wyckoff(){
        if(wyckoff != NULL){
          delete [] wyckoff; 
          wyckoff = NULL;
        }
      }
    
    };





    // parameters to be stored in the object
    int spaceGroupNumber;
    int num_spaceGroups;
    struct_wyckoff_entry *wyckoff_entry;
    struct_crystal_coordinates *crystal_coordinate_entry;
    struct_symmetric_wyckoff_pairs **symmetric_wyckoff_pairs;
    string symmetric_wyckoff_pairs_filename;
    
    string trigonal_flag;
    int num_lattice_parameters, num_basis_parameters, num_parameters;
    string *lattice_parameters, *basis_parameters, *parameters;
    
    int num_wyckoff;     
    string *wyckoff;

    int num_species;
    string *species;  
    string name;

    int num_wyckoff_orders;     
    int desired_wyckoff_order_id;
    string *wyckoff_orders;
    
    int num_species_permutations;
    string *species_permutations;  
    
    string structure_file_content;
    LATTICE *lattice, *primitive_lattice, *std_lattice;
   
    SpglibDataset *symmetry_dataset; 
    
    double *lattice_parameter_values, *basis_parameter_values, 
                                                           *parameter_values;
     
    // functions
    void read_Wyckoff_data();
    void set_crystal_class_coordinates();
    void read_vdw_data();
    void generate_symmetric_wyckoff_pairs();
    void dump_symmetric_wyckoff_pairs();
    bool read_symmetric_wyckoff_pairs();
    void calculate_lattice_parameters();
    void calculate_basis_parameters();
    void reset_spacegroup();
    void reset_wyckoff();
    void reset_species();
    void reset_parameter_values();
    void calculate_parameters();
    SpglibDataset* calculate_symmetry_dataset(LATTICE *lattice);
    void calculate_parameters_default_values(double *wyckoff_coordinates);
    void calculate_lattice_parameters_default_values();
    void calculate_basis_parameters_default_values(double *wyckoff_coordinates);
    void calculate_lattice();
    void calculate_std_or_primitive_lattice(LATTICE **lattice, int flag);
    void get_alat_permutations(int *npermu, double **permu);
    bool if_valid_cellpar_angles(double *cellpar);
    bool if_valid_lattice_parameters_as_per_standardization(double *cellpar);
    string get_symmetry_symbol_of_wyckoff_site(string site);
    bool if_symmetric_wyckoff_pairs(string soder1, string sorder2);
    void get_wyckoff_identity(int *wyckoff_identity); 
    string get_name_string_from_given_wyckoff_order(string *wyckoff_order);
    void generate_name_and_wyckoff_orders();
    void generate_species_permutations();
    
    void set_lattice_from_parameter_values(int num, string *name, double *value, 
                                                      bool reset = true);
    
    void get_symmetric_wyckoff_pairs(string given_order, 
                                              int *num, string **orders);

    bool if_wyckoff_order_related_through_symmetry(int num_wyckoff, 
                                                   string order1, string order2,
                                                   bool screen = true,
                                                   double *rotation = NULL,
                                                   double *translation = NULL);
     
    void get_wyckoff_site(string wyckoff_site, int *num_entry, double **entry,
                                     double *permu, string allign_wyckoff_site);
    
    bool if_wyckoff_site_related_through_offset(int ndist, double *dist1, 
                                            double *dist2, double *origin_shift,
                                            int flip=0);
   
    void get_specie_wyckoff_from_wyckoff_order(
                                      string *wyckoff_order,
                                      int *ntype,
                                      struct_specie_wyckoff **specie_wyckoff);

    void handle_wyckoff_of_same_identity(int *wyckoff_identity,
                                                  int identity,
                                                  int *num, 
                                                  string **wyckoff_list);
    
    void generate_valid_wyckoff_permutations_list(int *wyckoff_identity, 
                                            int current_identity,
                                            int *num, string **wyckoff_list);

  public:
    
    // setters
    void set_spacegroup(int spaceGroupNumber, bool reset = true);
    void set_wyckoff(int num, string *wyckoff, bool reset = true); 
    void set_species(int num, string *species, bool reset = true);
    void set_structure_from_file(string file_content, 
                                          bool reset_lattice = true);
    void set_parameter_values(int num, string *name, double *value);
    
    // getters
    int get_spacegroup();
    void get_species(int *num, string **species);
    void get_wyckoff(int *num, string **wyckoff);
    void get_wyckoff_list(int *num, int **multiplicity, int **if_repeatable,
                                                        string **site_symbol);
    void get_parameters(int *num, string **parameters);
    void get_parameter_values(int *num, string **name, double **value);
    void get_name(string *name);
    void get_species_permutations(int *num, string **permutations);
    string get_std_poscar();
    string get_primitive_poscar();
    int get_std_natom();
    int get_primitive_natom();
   
    int get_std_primitive_natom_ratio();
    
    SYSTEM_BULK(double tolerance=1e-3, string path = "./");
    ~SYSTEM_BULK(); 


};



#endif
