#include "system_common.h" 

// ---------------------------------------------------------------------------//
// get "cartesian product" of vector of vectors
// similar to python function itertools.product()
void SYSTEM_COMMON::get_cartesian_product(int num, string *vec, int *size, int
        *nproduct, string **product){
  // num is number of vectors; same as size of start
  // vec is array of strings
  // size is the size of each vec
  // vec = [v1 v1 v1 v2 v2 v3 v3 v4 v4 v4 v4]
  // size = [3 2 2 4]
  // num = 4

  if(num == 1){
    *nproduct = size[0];
    *product = new string [*nproduct];
    for(int i=0; i<*nproduct; i++){
      (*product)[i] = vec[i];
    }
    return;
  }

  int local_nproduct;
  string *local_product = NULL;
  get_cartesian_product(num-1, &vec[size[0]], &size[1], &local_nproduct, 
                            &local_product);
 
  *nproduct = size[0] * local_nproduct;
  *product = new string [*nproduct];
  for(int i=0; i<size[0]; i++){
    for(int j=0; j<local_nproduct; j++){
      (*product)[i*local_nproduct + j] = vec[i] + " " + local_product[j];  
    }
  }

  if(local_product != NULL){
    delete [] local_product;
    local_product = NULL;
  }
 
}


// ---------------------------------------------------------------------------//
// solve system of linear equations with possibly reduced matrix rank
void SYSTEM_COMMON::solve_linear_system_of_reduced_rank(double *A, double *C, 
                         double *X, int num_eqn, int num_unknown, int *rank, 
                         bool *if_solution, bool if_solve){
 
  // A: coeff matrix
  // C: rhs
  // X: solution
  // if_solve decides whether to solve it or to return U matrix


  if( !if_solve ){
    // get only the U matrix and rearrange A

    int num_rows  = (num_unknown > num_eqn) ? num_eqn : num_unknown;
    MatrixXd AA(num_eqn, num_unknown);
    MatrixXd U(num_rows, num_unknown);
    MatrixXd UQ(num_rows, num_unknown);
    for(int i=0;i<num_eqn;i++){
      for(int j=0;j<num_unknown;j++){
        AA(i,j) = A[num_unknown*i + j];
      }
    }
    
    U = AA.fullPivLu().matrixLU().triangularView<Upper>();
    UQ = U * AA.fullPivLu().permutationQ().inverse(); 
    *rank = AA.fullPivLu().rank();

    for(int i=0; i<num_eqn*num_unknown; i++) A[i] = 0.0;
    for(int i=0; i<num_rows; i++){
      for(int j=0; j<num_unknown; j++){
        A[num_unknown*i+j] = UQ(i,j);
      }
    }

  

    return; 
  }

  if(if_solve){
    MatrixXd AA(num_eqn, num_unknown);
    VectorXd CC(num_eqn);
    VectorXd XX(num_unknown);
    for(int i=0;i<num_eqn;i++){
      for(int j=0;j<num_unknown;j++){
        AA(i,j) = A[num_unknown*i + j];
      }
      CC(i) = C[i];
    }
    XX = AA.fullPivLu().solve(CC);
    *rank = AA.fullPivLu().rank();

    for(int i=0; i<num_unknown; i++) X[i] = XX(i); 
  
    *if_solution = true;
    double diff;
    for(int i=0; i<num_eqn; i++){
      diff = 0.0;
      for(int j=0; j<num_unknown;j++) diff += A[num_unknown*i+j] * X[j];
      diff -= C[i];
      diff -= round(diff);
      if(abs(diff) > this->tolerance){
        *if_solution = false;
        break;
      }
    }
  }

  
}




// ---------------------------------------------------------------------------//
// solve system of linear equations with possibly reduced matrix rank
void SYSTEM_COMMON::solve_linear_system_of_reduced_rank_with_modulii_one(
                          double *A, 
                          double *C, double *X, bool *if_solution_found){
 
  // A: coeff matrix, must be square
  // C: rhs
  // X: solution
  // num: |C| or |X|

  int num = 3;
  MatrixXd AA(num, num);
  VectorXd CC(num);
  VectorXd XX(num);
  for(int i=0;i<num;i++){
    for(int j=0;j<num;j++){
      AA(i,j) = A[num*i + j];
    }
    CC(i) = C[i];
  }
  int rank = AA.fullPivLu().rank();

  for(int d=0; d<3; d++) X[d] = 0.0;
  
  // rank 1 
  if(rank == 1){
    
    // find the right equation
    int i1 = -1;;
    for(int i=0; i<num; i++){
      for(int d=0;d<3;d++){
        if(abs(A[num*i+d]) > this->tolerance){
          i1 = i;
          break;
        }
      }
      if(i1 != -1) break;
    }
   
    // copy the same equation num times in AA
    for(int i=0; i<num; i++){
      for(int j=0; j<num; j++){
        AA(i,j) = A[num*i1 + j];
      }
      CC(i) = C[i1];
    }

    XX = AA.fullPivLu().solve(CC);
    for(int i=0; i<num; i++) X[i] = XX(i); 

  }


  // rank 2
  if(rank == 2){
    
    // find right equations
    int i1;
    bool if_zero;
    i1 = -1; 
    
    // check if any equationis zero
    for(int i=0; i<num; i++){
      if_zero = true;
      for(int d=0;d<3;d++){
        if(abs(A[num*i+d]) > this->tolerance){
          if_zero = false;
          break;
        }
      }
      if(if_zero){
        i1 = i;
        break;
      }
    }

    if(i1 == -1){
      // check if 0-1 involves different variables
      if_zero = true;
      for(int d=0;d<3;d++){
        if( ( (abs(A[num*0+d]) > this->tolerance)  && 
                              (abs(A[num*1+d]) < this->tolerance) ) ||
         ( (abs(A[num*0+d]) < this->tolerance)  && 
                              (abs(A[num*1+d]) > this->tolerance) )){
          if_zero = false;
        }
      }
      if(if_zero){
        i1 = 1;
      }
    }

    if(i1 == -1){
      // check if 0-2 involves different variables
      if_zero = true;
      for(int d=0;d<3;d++){
        if( ( (abs(A[num*0+d]) > this->tolerance)  && 
                              (abs(A[num*2+d]) < this->tolerance) ) ||
         ( (abs(A[num*0+d]) < this->tolerance)  && 
                              (abs(A[num*2+d]) > this->tolerance) )){
          if_zero = false;
        }
      }
      if(if_zero){
        i1 = 2;
      }
    }

    if(i1 == -1){
      // check if 1-2 involve different variables
      if_zero = true;
      for(int d=0;d<3;d++){
        if( ( (abs(A[num*1+d]) > this->tolerance)  && 
                              (abs(A[num*2+d]) < this->tolerance) ) ||
         ( (abs(A[num*1+d]) < this->tolerance)  && 
                              (abs(A[num*2+d]) > this->tolerance) )){
          if_zero = false;
        }
      }
      if(if_zero){
        i1 = 2;
      }
    }



    if(i1 == -1){
      cout<<"Oops! cannot solve the equations!"<<'\n';
      return;
    }

    for(int i=0; i<num; i++){
      for(int j=0; j<num; j++){
        AA(i,j) = A[num*i + j];
      }
      CC(i) = C[i];
    }
    for(int d=0;d<num;d++){
      AA(i1, d) = 0.0;
    }
    CC(i1) = 0.0;
    
    XX = AA.fullPivLu().solve(CC);
    for(int i=0; i<num; i++) X[i] = XX(i); 

  }

  // rank 3  
  if(rank == 3){
    XX = AA.fullPivLu().solve(CC);
    for(int i=0; i<num; i++) X[i] = XX(i); 
  }
 

  *if_solution_found = true;
  double temp;
  for(int i=0; i<num; i++){
    temp = 0.0;
    for(int d=0;d<num;d++) temp+= A[num*i+d] * X[d];
    temp -= C[i];
    temp -= round(temp);
    if(temp < -this->tolerance) temp += 1.0;
    if(abs(temp) > this->tolerance){
      *if_solution_found = false;
      return; 
    }
  }
    

}


// ---------------------------------------------------------------------------//
// map from atomic_symbol to atom_id and vice-versa
void SYSTEM_COMMON::map_atomic_symbol_id(int *id, string *symbol, int flag){
  // flag = -1 -> map from id to string
  // flag = 1 -> map from string to id

  if(flag == 1){
    *id = 0;
    int c;
    for(unsigned int i=0; i<(*symbol).length(); i++){
      c = (*symbol)[i];
      *id = 200*(*id) + c;
    }
    return;
  }
  
  if(flag == -1){
    *symbol = "";
    int temp_id = *id;
    char c;
    while(temp_id > 200){
      c = temp_id / 200;
      *symbol += c;
      temp_id -= 200*(temp_id / 200);          
    }
    *symbol += temp_id;
    return;
  }
    
}


// ---------------------------------------------------------------------------//
// get cellpar from alat and vice versa
void SYSTEM_COMMON::calculate_cellpar_from_alat(double *alat, 
                                                  double *cellpar, int flag){
  // if flag == 1, convert from alat to cellpar
  // if flag ==-1, covert from cellpar to alat

  double pi = 4.0*atan(1.0);

  if(flag == 1){
    for(int i=0; i<6; i++) cellpar[i] = 0.0;
    
    // set magn first
    for(int i=0;i<3; i++){
      for(int d=0; d<3; d++)  cellpar[i] += alat[3*i+d]*alat[3*i+d];     
      cellpar[i] = sqrt(abs(cellpar[i]));
    }

    // get angles now
    for(int d=0; d<3; d++){
      cellpar[3] += alat[3*1+d]*alat[3*2+d];     
      cellpar[4] += alat[3*0+d]*alat[3*2+d];     
      cellpar[5] += alat[3*0+d]*alat[3*1+d];     
    }    
    cellpar[3] /= (cellpar[1] * cellpar[2]);
    cellpar[4] /= (cellpar[0] * cellpar[2]);
    cellpar[5] /= (cellpar[0] * cellpar[1]);
  
    //convert to degrees now
    cellpar[3] = 180. * (acos(cellpar[3]) / pi);
    cellpar[4] = 180. * (acos(cellpar[4]) / pi);
    cellpar[5] = 180. * (acos(cellpar[5]) / pi);
  
    // now convert lengths to ratios
    cellpar[1] /= cellpar[0];
    cellpar[2] /= cellpar[0];

  }

  
  if(flag == -1){
    // adapted from lammps website for triclinic box

    for(int i=0; i<9; i++) alat[i] = 0.0;

    // convert relative lengths to lengths
    cellpar[1] *= cellpar[0];
    cellpar[2] *= cellpar[0];

    //convert angles to radians
    cellpar[3] *= pi/180.;
    cellpar[4] *= pi/180.;
    cellpar[5] *= pi/180.;

    // v1
    alat[3*0+0] = cellpar[0];
    alat[3*0+1] = 0.0;
    alat[3*0+2] = 0.0;

    // v2
    alat[3*1+0] = cellpar[1]*cos(cellpar[5]);
    alat[3*1+1] = cellpar[1]*sin(cellpar[5]);
    alat[3*1+2] = 0.0;

    // v3
    alat[3*2+0] = cellpar[2]*cos(cellpar[4]);
    alat[3*2+1] = (cellpar[1]*cellpar[2]*cos(cellpar[3]) - alat[3*1+0] *
                                  alat[3*2+0]) / (alat[3*1+1]) ;
    alat[3*2+2] = sqrt(abs(cellpar[2]*cellpar[2] - alat[3*2+0]*alat[3*2+0] -
                                                alat[3*2+1]*alat[3*2+1]));
    
    
    cellpar[1] /= cellpar[0];
    cellpar[2] /= cellpar[0];
    cellpar[3] /= pi/180.;
    cellpar[4] /= pi/180.;
    cellpar[5] /= pi/180.;

  }

}








// ---------------------------------------------------------------------------//
// get Rotation matrix form angle and axis
void SYSTEM_COMMON::get_rotation_matrix(double *axis, double theta, double *R){
  double ct, st;
  double ux, uy, uz;
  ct = cos(theta);
  st = sin(theta);
  ux = axis[0]; uy = axis[1]; uz = axis[2];
  
  R[3*0+0] = ct + ux*ux*(1-ct);
  R[3*0+1] = ux*uy*(1-ct) - uz*st;
  R[3*0+2] = ux*uz*(1-ct) + uy*st;

  R[3*1+0] = uy*ux*(1-ct) + uz*st;
  R[3*1+1] = ct + uy*uy*(1-ct);
  R[3*1+2] = uy*uz*(1-ct) - ux*st;

  R[3*2+0] = uz*ux*(1-ct) - uy*st;
  R[3*2+1] = uz*uy*(1-ct) + ux*st;
  R[3*2+2] = ct + uz*uz*(1-ct);

}
  
  
  
// ---------------------------------------------------------------------------//
// get symmetry opertaions
void SYSTEM_COMMON::calculate_symmetry_rotations_translations(LATTICE *lattice, 
      double **rotations, double **translations, int *num_sym){
 
  if(*rotations != NULL){
    delete [] (*rotations);
    *rotations = NULL;
  }
  if(*translations != NULL){
    delete [] (*translations);
    *translations = NULL;
  }
  
  
  double a[3][3];
  double (*position)[3] = new double [lattice->natom][3];
  int *types;
  types = new int [lattice->natom];

  for(int j=0;j<3;j++){
    for(int d=0;d<3;d++){
      a[d][j] = lattice->alat[3*j+d];
    }
  }
  for(int i=0;i<lattice->natom;i++){
    for(int d=0;d<3;d++){
      position[i][d] = lattice->basis[i].direct[d];
    }
    map_atomic_symbol_id(&types[i], 
                            &(lattice->basis[i].symbol), 1);
  }

  
  int tmp_num_sym = spg_get_multiplicity(a, position, types, 
                                        lattice->natom, this->tolerance);
  
  int (*tmp_rotations)[3][3] = new int [tmp_num_sym][3][3];
  double (*tmp_translations)[3] = new double [tmp_num_sym][3];
  tmp_num_sym = spg_get_symmetry(tmp_rotations, tmp_translations, tmp_num_sym, 
            a, position, types, lattice->natom, this->tolerance);

  *num_sym = tmp_num_sym;
  *rotations = new double [3*3*(*num_sym)];
  *translations = new double [3*(*num_sym)];
  for(int i=0; i<(*num_sym); i++){
    for(int d1=0;d1<3;d1++){
      for(int d2=0;d2<3;d2++){
        (*rotations)[9*i + 3*d1 + d2] = tmp_rotations[i][d1][d2]; 
      }
      (*translations)[3*i+d1] = tmp_translations[i][d1];
    }
  }

  delete [] tmp_rotations; tmp_rotations = NULL;
  delete [] tmp_translations; tmp_translations = NULL;
  delete [] position; position = NULL;
  delete [] types;  types = NULL;

}





// ---------------------------------------------------------------------------//
// see if two sets of given direct coordinate are related through given symmetry
bool SYSTEM_COMMON::if_sites_related_through_given_symmetry(
                                              double *p1, double *p2,
                                              double *R, double *T){
  double temp[3];
  for(int d=0;d<3;d++){
    temp[d] = 0.0;
    for(int i=0;i<3;i++){
      temp[d] += R[3*d+i] * p1[i];
    }
    temp[d] += T[d];
  }
 
  double temp_p2[3];
  for(int d=0;d<3;d++){
    temp[d] = temp[d] - round(temp[d]);
    if(temp[d] < -this->tolerance) temp[d] += 1.0;
    temp_p2[d] = p2[d] - round(p2[d]);
    if(temp_p2[d] < -this->tolerance) temp_p2[d] += 1.0;

  }
  
  
  for(int d=0; d<3; d++){
    if(abs(temp_p2[d] - temp[d]) > this->tolerance){
      return false;
    }
  }

  return true;
}


