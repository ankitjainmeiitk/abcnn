#ifndef __COMMON_H
#define __COMMON_H

//---- standard header files ---------
#ifndef __STANDARD_H
#define __STANDARD_H
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string.h>
#include <sstream>
#include <math.h>
#include <stdio.h>
#include <ctime>
#include <cstdlib>
#include <complex>
#include <sys/stat.h>
#include <algorithm>
#include <set>
using namespace std;
#endif

#ifndef __SPGLIB_H
#define __SPGLIB_H
extern "C"  {
  #include <spglib.h>
}
#endif

#ifndef __EIGEN_HEADER__
#define __EIGEN_HEADER__
#include "./../include/Eigen/Dense"
using namespace Eigen;
#endif


struct ATOM{
  double direct[3];
  double cart[3];
  string symbol;
  bool fix[3];

  ATOM(){
    for(int d=0;d<3;d++){
      direct[d] = 0.0;
      cart[d] = 0.0;
      fix[d] = false;
    }
  }

};

struct LATTICE{
  ATOM *basis;
  double alat[9];
  double inv_alat[9];
  int natom;
 
  LATTICE(){
    basis = NULL;
    natom = 0;
  }
  
  ~LATTICE(){
    if(basis != NULL){
      delete [] basis;
      basis = NULL;
    }
  }
};

// general methods
string get_progress_string(double total_progress, double current_progress);
void split_string_into_string_array(string str, int num, string **array);
int calculate_gcd(int a, int b);
string int_to_str(int i);
string float_to_str(float f);
void calculate_cross_product(double *a, double *b, double *c);
void calculate_matrix_inverse(double *a, double *b);
void calculate_matmul(double *A, double *B, double *C);
void n_choose_k_int(int n, int k, int *list, int *nentry, int **entry);
double get_random_double_in_range(double given_min, double given_max);
int get_random_int_in_range(int given_min, int given_max);

// lattice methods
void set_inv_alat(LATTICE *lattice);
void cart_to_direct(LATTICE *lattice, int flag);
void create_supercell(LATTICE *lattice, LATTICE **superlattice, int *N);
void read_structure(string file_content, LATTICE *lattice, 
                                                  double tolerance = 1e-3);
void copy_lattice(LATTICE *l1, LATTICE **l2);
string get_poscar_from_lattice(LATTICE *lattice, 
                                            bool selective_dynamics = false);
double find_distance_square_between_atoms(ATOM a1, ATOM a2);
    

#endif
