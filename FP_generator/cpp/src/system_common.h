#ifndef __SYSTEM_COMMON_H 
#define __SYSTEM_COMMON_H 

#include "common.h"



class SYSTEM_COMMON{
  private:

  protected:

    double tolerance;
    string data_directory;

    void get_cartesian_product(int num, string *vec, int *size, int
                  *nproduct, string **product);
    
    void solve_linear_system_of_reduced_rank(double *A, double *C, 
                       double *X, int num_eqn, int num_unknown, int *rank, 
                       bool *if_solution, bool if_solve=true);
    
    void solve_linear_system_of_reduced_rank_with_modulii_one(double *A, 
                        double *C, double *X, 
                        bool *if_solution);
    
    void map_atomic_symbol_id(int *id, string *symbol, int flag);
    void calculate_cellpar_from_alat(double *alat, double *cellpar, int flag);
    void get_rotation_matrix(double *axis, double theta, double *R);
    void calculate_symmetry_rotations_translations(LATTICE *lattice, 
              double **rotations, double **translations, int *num_sym); 
    bool if_sites_related_through_given_symmetry(double *p1, double *p2,
                                                        double *R, double *T);

  public:
    

};


#endif
