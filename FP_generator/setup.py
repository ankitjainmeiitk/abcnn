import os
#from distutils.core import setup, Extension
from setuptools import setup, Extension

bulk_enumerator = Extension('bulk_enumerator/_bulk_enumerator',
                            include_dirs=[
                                'cpp/include/spglib',
                            ],
                            sources=[
                                'cpp/include/spglib/arithmetic.c',
                                'cpp/include/spglib/cell.c',
                                'cpp/include/spglib/debug.c',
                                'cpp/include/spglib/delaunay.c',
                                'cpp/include/spglib/determination.c',
                                'cpp/include/spglib/hall_symbol.c',
                                'cpp/include/spglib/kgrid.c',
                                'cpp/include/spglib/kpoint.c',
                                'cpp/include/spglib/mathfunc.c',
                                'cpp/include/spglib/niggli.c',
                                'cpp/include/spglib/pointgroup.c',
                                'cpp/include/spglib/primitive.c',
                                'cpp/include/spglib/refinement.c',
                                'cpp/include/spglib/site_symmetry.c',
                                'cpp/include/spglib/sitesym_database.c',
                                'cpp/include/spglib/spacegroup.c',
                                'cpp/include/spglib/spg_database.c',
                                'cpp/include/spglib/spglib.c',
                                'cpp/include/spglib/spglib_f.c',
                                'cpp/include/spglib/spin.c',
                                'cpp/include/spglib/symmetry.c',
                                'cpp/include/spglib/tetrahedron_method.c',
                                'cpp/src/common.cpp',
                                'cpp/src/system_common.cpp',
                                'cpp/src/system_bulk.cpp',
                                'cpp/src/system_bulk_c.cpp',
                            ]
                            )

setup(
    name='BulkEnumerator',
    version='0.0.2',
    packages=['bulk_enumerator'],
    package_data={'bulk_enumerator' : [
                    './DATA/covRadii.dat',
                    './DATA/vDW.dat',
                    './DATA/Wyckoff.dat',
                    './DATA/Symmetric_Wyckoff_Pairs.dat',
    ]},
    author='Ankit Jain',
    author_email='ankitj@alumni.cmu.edu',
    license='Copyright Ankit Jain',
    description='Create arbitrary atomic bulk crystal structures',
    ext_modules=[bulk_enumerator]
)
