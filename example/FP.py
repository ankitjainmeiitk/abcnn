import bulk_enumerator as be
from collections import defaultdict
import ase                                                                          
import ase.atom 

wyckoffs_map = defaultdict(int)                                                     
alphabets = "abcdefghijklmnopqrstuvwxyzA"                                           
for i in range(len(alphabets)):                                                     
  wyckoffs_map[alphabets[i]] = i+1                                                  
                                                                                    
atoms_map = defaultdict(int)                                                        
for i in range(1,110):                                                              
  symbol = ase.atom.Atom(i).symbol                                                  
  atoms_map[symbol] = i  

fp = open("fingerprint.dat", "w+")

with open("properties.dat", "r") as f:
  for line in f:
    val = line.split()
    filename = val[0]
    energy = float(val[1])

    with open(filename, "r") as ftemp:
      poscar = ftemp.read()

    # extract properties  
    b = be.bulk.BULK()
    b.set_structure_from_file(poscar)
    name = b.get_name()
    spacegroup = b.get_spacegroup()
    wyckoffs = b.get_wyckoff()
    species = b.get_species()
    b.delete()
    
    # sort in alphabetical order of Wyckoffs
    data = []
    for w,s in zip(wyckoffs, species):
      data.append({'wyckoff' : w, 'specie' : s})
    data.sort(key = lambda entry: entry['wyckoff'])  
    wyckoffs = []
    species = []
    for entry in data:
      wyckoffs.append(entry['wyckoff'])
      species.append(entry['specie'])
    wyckoffs = "_".join([str(wyckoffs_map[_]) for _ in wyckoffs])                
    species = "_".join([str(atoms_map[_]) for _ in species])  


    fp.write("%s %d %s %s %lf\n" % (name, spacegroup, wyckoffs, species, energy))

fp.close()
